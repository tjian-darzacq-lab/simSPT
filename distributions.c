#include "distributions.h"

double exponential(double mean, gsl_rng *prng) {
  return -log(gsl_rng_uniform(prng))*mean;
}

double gaussrand() {// from http://c-faq.com/lib/gaussian.html
  // We are using the Marsaglia Polar method. Note that the Ziggurat method is
  // even more efficient
  // This is deprecated  
  
  static double V1, V2, S;
  static int phase = 0;
  double X;
  
  if(phase == 0) {
    do {
      double U1 = (double)rand() / RAND_MAX;
      double U2 = (double)rand() / RAND_MAX;
      
      V1 = 2 * U1 - 1;
      V2 = 2 * U2 - 1;
      S = V1 * V1 + V2 * V2;
    } while(S >= 1 || S == 0);
    
    X = V1 * sqrt(-2 * log(S) / S);
  } else
    X = V2 * sqrt(-2 * log(S) / S);
  
  phase = 1 - phase;
  
  return X;
}

double gaussrand_zig(gsl_rng *prng) { // Simple wrapper for ziggurat method in GSL
  return gsl_ran_gaussian_ziggurat(prng, 1.0);
}

double power(double a, double b, double g, gsl_rng *prng) {
  //Power-law gen for pdf(x)\propto x^{g-1} for a<=x<=b
  //  From: https://stackoverflow.com/questions/31114330/python-generating-random-numbers-from-a-power-law-distribution#31117560
  // Python
  //r = np.random.random(size=size)
  double r = gsl_rng_uniform(prng);
  double ag = pow(a, g);
  double bg = pow(b, g);
  //ag, bg = a**g, b**g
  //return (ag + (bg - ag)*r)**(1./g)

  return pow(ag + (bg - ag)*r, 1./g);
}
