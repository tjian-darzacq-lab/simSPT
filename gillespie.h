#include <stdio.h>
#include <stdlib.h>
#include <gsl/gsl_math.h>
#include <gsl/gsl_eigen.h>
#include <gsl/gsl_rng.h>

int idx(int src, int dst, int np);
int get_equilibrium(double popsK[], int n_pops, struct diffusionPars *pops);
void gillespie_move(float *t, float *tt, struct point *p,
		    int *curr_pop, int *old_pop,
		    float dt, struct diffusionPars *pops, double popsK[],
		    int n_pops, gsl_rng *prng);
