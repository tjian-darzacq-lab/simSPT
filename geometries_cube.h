#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <gsl/gsl_rng.h>

bool cube_point_in_geometry(struct point p, struct geometryInit pars);

// This is actually never used :(
struct point cube_get_random_initialization_point(struct point p,
						  struct geometryInit pars,
						  gsl_rng *prng);
struct geometryInit cube_initialize_geometry(struct geometryInit ret, struct geometryPars pars);
struct point cube_confine_translocation(struct point pp, struct point cp,
					  struct geometryInit geometry_init);
