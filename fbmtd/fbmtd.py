# A wrapper for circulant.c
# Compilation instructions in the Makefile.
# By MW, GPLv3+, Aug. 2018

import sys
import random
import numpy as np
from ctypes import cdll, c_double, c_int, c_long, POINTER

hoskinglib = cdll.LoadLibrary("./fbm.so")


def circulant(n, H, L, cum=1, seed1=None, seed2=None):
    """A python wrapper around the C implementation of the Hosking method"""
    if seed1 is None:
        seed1 = random.randrange(2**31-1)
    if seed2 is None:
        seed2 = random.randrange(2**31-1)
    r = (c_double*(2**n))()
    hoskinglib.circulant_wrapper.argtypes = [c_long, c_double, c_double,
                                             c_int, c_long, c_long,
                                             POINTER(c_double)]
    hoskinglib.circulant_wrapper(n, H, L, cum, seed1, seed2, r)

    return np.array(list(r))


def hosking(n, H, L, cum=1, seed1=None, seed2=None):
    """A python wrapper around the C implementation of the Hosking method"""
    if seed1 is None:
        seed1 = random.randrange(2**31-1)
    if seed2 is None:
        seed2 = random.randrange(2**31-1)
    r = (c_double*(2**n))()
    hoskinglib.hosking_wrapper.argtypes = [c_long, c_double, c_double,
                                           c_int, c_long, c_long,
                                           POINTER(c_double)]
    hoskinglib.hosking_wrapper(n, H, L, cum, seed1, seed2, r)

    return np.array(list(r))


if __name__ == "__main__":
        circulant(10, 0.2, 10)
