// A small main

#include "hosking.h"
#include "circulant.h"
//#include <stdio.h>

long n = 10;
double H = 0.2;
double L = 10.0;
int cum = 1;
long seed1 = 1;
long seed2 = 42;
double output[1024];
int i;

int hosking_wrapper(long n, double H, double L, int cum, 
		    long seed1, long seed2, double *output) {
  hosking(&n, &H, &L, &cum, &seed1, &seed2, output);

  return 2;
}

int circulant_wrapper(long n, double H, double L, int cum, 
		    long seed1, long seed2, double *output) {
  circulant(&n, &H, &L, &cum, &seed1, &seed2, output);

  return 2;
}


/* int main() { */
/*   printf("Compiled as a shared library.\n"); */
/*   //hosking(&n, &H, &L, &cum, &seed1, &seed2, output); */

/*   //for (i=0; i<1024; i++) { */
/*   //  printf("%f\n", output[i]); */
/*   //} */
  
/*   return 0; */
/* } */
