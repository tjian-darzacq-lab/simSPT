#include "simSPT_structs.h"
#include "geometries_sphere.h"
#include "z_profiles.h"
bool sphere_point_in_geometry(struct point p, struct geometryInit pars) {
  if (pow(p.x, 2) + pow(p.y, 2) + pow(p.z, 2) > pow(pars.sphere_radius, 2)) {
    return false;
  } else {
    return true;
  }
}

struct point sphere_get_random_initialization_point(struct point p,
						    struct geometryInit pars,
						    gsl_rng *prng, bool sim2d) {
  // Generate a value
  p.x = pars.max_x+1;
  p.y = 0;
  p.z = 0;
  while (sphere_point_in_geometry(p, pars)==false) {
    p.x = (pars.max_x-pars.min_x)*(float)gsl_rng_uniform(prng) + pars.min_x;
    p.y = (pars.max_y-pars.min_y)*(float)gsl_rng_uniform(prng) + pars.min_y;
    p.z = (pars.max_z-pars.min_z)*(float)gsl_rng_uniform(prng) + pars.min_z;
    if (sim2d) {
      p.z=0;
    }
  }
  return p;
}

struct geometryInit sphere_initialize_geometry(struct geometryInit ret, struct geometryPars pars) {
  float r = pars.radius;
  ret.max_x=r;
  ret.min_x=-r;
  ret.max_y=r;
  ret.min_y=-r;
  ret.max_z=r;
  ret.min_z=-r;
  ret.sphere_radius=r;
  return ret;
}

point ip, ipn, dirn, refn;// Intersection between the segment and the sphere,
                          // the same normalized, direction vector normalized,
                          // the reflected vector normalized

double ipnc, dirnc, dotp, scl; // normalization constant for ipn, for dirn,
                               // a dot product, the scale of the new vector

point center = {0,0,0};
bool result;
double mu1, mu2, mu; // See RaySphere docstring
bool debug = false; // When compilation is run with this 
double lt,l1,l2, l2r; // Debug variable

struct point sphere_confine_translocation(struct point pp, struct point cp,
					  struct geometryInit gi, gsl_rng *prng) {

  // If the point is inside the geometry, there is nothing to do.
  if (sphere_point_in_geometry(cp, gi)==true) {
    return cp;
  }
  
  // This should never be executed!!!
  if (sphere_point_in_geometry(pp, gi)==false) {
    pp = sphere_get_random_initialization_point(pp, gi, prng, false);
    printf("(Should not happen)Initial point is outside the sphere. Rerunning\n");
    printf("BUG: Not ready for 2D sim");
    // panic mode
    if (sphere_point_in_geometry(pp, gi)==false) {
      printf("(Should never happen)Initial point still outside sphere!!!!\n");
      exit(1);
    }
  }
  
  int i=0;
  while (sphere_point_in_geometry(cp, gi)==false) {
    result = RaySphere(pp, cp, center, gi.sphere_radius, &mu1, &mu2);
    if (result==true) { // It worked, we have the intersection
      
      // Compute the intersection point
      mu = fmax(mu1, mu2);
      //printf("%f %f %f\n", mu1, mu2, mu); // DBG
      ip.x = (pp.x + mu*(cp.x-pp.x)); 
      ip.y = (pp.y + mu*(cp.y-pp.y));
      ip.z = (pp.z + mu*(cp.z-pp.z));
      scl = sqrt(pow(cp.x-ip.x,2)+pow(cp.y-ip.y,2)+pow(cp.z-ip.z,2)); // Magnitude of the new vector
      
      // Check
      if (mu>1) {
	printf("(should never happen) mu=%f > 1", mu);
      }

      // Debug stuff: make sure we compute the intersection point right
      if (debug) {
	// Compare the length of the total vector with the sum of the two segments
	lt = sqrt(pow(pp.x-cp.x,2)+pow(pp.y-cp.y,2)+pow(pp.z-cp.z,2));
	l1 = sqrt(pow(pp.x-ip.x,2)+pow(pp.y-ip.y,2)+pow(pp.z-ip.z,2));
	l2 = scl; // Second segment
	printf("tot=%f l1=%f l2=%f | l1+l2-tot=%f | ", lt, l1, l2, l1+l2-lt);
	printf("err>eps?: %s ", fabs(l1+l2-lt)>(double)DBL_EPSILON*10 ? "Y" : "N");
	if (fabs(l1+l2-lt)>(double)FLT_EPSILON) {
	  printf("Error too high, exiting");
	  exit(1);
	}
      }

      // Computing the normal & normalize direction vector (ipn, dirn)
      ipnc = sqrt(pow(ip.x,2)+pow(ip.y,2)+pow(ip.z,2));
      ipn.x = ip.x/ipnc;
      ipn.y = ip.y/ipnc;
      ipn.z = ip.z/ipnc;
      dirn.x = cp.x-pp.x;
      dirn.y = cp.y-pp.y;
      dirn.z = cp.z-pp.z;
      dirnc = sqrt(pow(dirn.x,2)+pow(dirn.y,2)+pow(dirn.z,2));
      dirn.x /= dirnc;
      dirn.y /= dirnc;
      dirn.z /= dirnc;

      // Compute specular reflection (and scale to the right magnitude)
      dotp = ipn.x*dirn.x + ipn.y*dirn.y + ipn.z*dirn.z;
      cp.x = (2*dotp*dirn.x-ipn.x)*scl+ip.x;
      cp.y = (2*dotp*dirn.y-ipn.y)*scl+ip.y;
      cp.z = (2*dotp*dirn.z-ipn.z)*scl+ip.z;

      // Update the previous point as the intersection point,so that we can iterate
      pp = ip;
      
      // Debug stuff: make sure that the scaling are conserved after the reflection
      if (debug) {
	l2r = sqrt(pow(cp.x-ip.x,2)+pow(cp.y-ip.y,2)+pow(cp.z-ip.z,2));
	printf("| l2r=%f, diff=%f |", l2r, l2-l2r);
	printf(" diff>eps? %s\n", fabs(l2-l2r)>10*DBL_EPSILON ? "Y" : "N");
	if (fabs(l2-l2r)>(double)FLT_EPSILON) {
	  printf("Error too high, exiting\n");
	  exit(1);
	}
      }
    } else {
      // If the algorithm found no intersection, we rely on randomly moving the
      // point by 0.1 nm. It is likely that this value could be much lower.
      printf("Something is wrong. Moving by a random value.\n");
      cp.x += (double)gaussrand_zig(prng)*0.0001;
      cp.y += (double)gaussrand_zig(prng)*0.0001;
      cp.z += (double)gaussrand_zig(prng)*0.0001;

      if (debug) {
	printf("gauss: %f", gaussrand_zig(prng));
	printf("%f %f %f (%f)|",pp.x,pp.y,pp.z,pow(pp.x,2)+pow(pp.y,2)+pow(pp.z,2));
	printf("%f %f %f (%f)\n",cp.x,cp.y,cp.z, pow(cp.x,2)+pow(cp.y,2)+pow(cp.z,2));
      }
    }
    
    // ==== DBG prints
    //printf("point: %f %f %f, radius: %f\n", cp.x, cp.y, cp.z,
    //        pow(cp.x,2)+pow(cp.y,2)+pow(cp.z,2)); // DBG
    //printf("%s\n", sphere_point_in_geometry(cp, gi) ? "true" : "false");

    // ==== Termination criterion if things go wrong
    i++;
    if (i>15) { 
      printf("Something is wrong, confinment looping forever?\n");
      if (i>100) {
	printf("It seems so, exiting\n");
	exit(1);
      }
    };
  }
  return cp;
};

// Helper functions

// Segment-Sphere intersection by Paul Bourke (1992)
// From: http://paulbourke.net/geometry/circlesphere/index.html#linesphere
// Edited by MW.
/*
   Calculate the intersection of a ray and a sphere
   The line segment is defined from p1 to p2
   The sphere is of radius r and centered at sc
   There are potentially two points of intersection given by
   p = p1 + mu1 (p2 - p1)
   p = p1 + mu2 (p2 - p1)
   Return FALSE if the ray doesn't intersect the sphere.
*/
bool RaySphere(point p1, point p2, point sc,double r,double *mu1,double *mu2)
{
   double a,b,c;
   double bb4ac;
   point dp;
   double EPS = DBL_EPSILON; // Might need to tune this value.

   dp.x = p2.x - p1.x;
   dp.y = p2.y - p1.y;
   dp.z = p2.z - p1.z;
   a = dp.x * dp.x + dp.y * dp.y + dp.z * dp.z;
   b = 2 * (dp.x * (p1.x - sc.x) + dp.y * (p1.y - sc.y) + dp.z * (p1.z - sc.z));
   c = sc.x * sc.x + sc.y * sc.y + sc.z * sc.z;
   c += p1.x * p1.x + p1.y * p1.y + p1.z * p1.z;
   c -= 2 * (sc.x * p1.x + sc.y * p1.y + sc.z * p1.z);
   c -= r * r;
   bb4ac = b * b - 4 * a * c;
   if (fabs(a) < EPS || bb4ac < 0) {
      *mu1 = 0;
      *mu2 = 0;
      return(false);
   }

   *mu1 = (-b + sqrt(bb4ac)) / (2 * a);
   *mu2 = (-b - sqrt(bb4ac)) / (2 * a);

   return(true);
}
