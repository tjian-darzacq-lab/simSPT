#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <float.h>
#include <string.h>
#include <gsl/gsl_rng.h>
int choose_a_population(struct diffusionPars *pops, int n_pops, gsl_rng *prng);
bool criterion(int stopCriterion, int stopCriterionValue,
	       int n_part, int n_traj, int n_det, int n_pos);
struct summaryStats make_trajectories(int stopCriterion, int stopCriterionValue,
		       struct diffusionPars *pops, int n_pops, double popsK[],
		       struct geometryInit geometry_init,
		       struct simulationPars simulation_params,
		       struct zprofileInit z_profile,
		       struct savePars save_params, struct hiloPars hilo_params,
		       struct summaryStats summary_stats, bool use_gillespie,
		       gsl_rng *prng);
bool detect_particle(struct detection p, struct zprofileInit z_profile);
bool particle_in_hilo_volume(struct detection cP, struct hiloPars hilo_params,
			     gsl_rng *prng);
