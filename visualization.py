#!/usr/bin/python
#-*-coding:utf-8-*-
## Small script to plot a 3D dataset
## This script requires Mayavi (that might be a little bit hard to install)
## By MW, GPLv3+, Jun 2017

## Imports
import sys, os
from mayavi import mlab
from mayavi.sources.builtin_surface import BuiltinSurface
import pandas as pd
import numpy as np

## ==== Variables
xmin,xmax=-.5, .5
nuclear_radius = 5.0

#maxtraj = 1000
## Load traces & convert them to a better format
#df = pd.read_csv("5k_export2.csv", index_col=0)
#traj = []
#for (i,g) in df.groupby("trajectory"):
#    traj.append(g)

if len(sys.argv)==1:
    print "Randomly generating an example of a trace"
    np.random.seed(0)
    Dsc = (2*4.0*0.02)**.5
    xyz = np.random.normal(scale=Dsc, size=(35,3))
    x = np.cumsum(xyz[:,0])-2
    y = np.cumsum(xyz[:,1])-2
    z = np.cumsum(xyz[:,2])

    involume = (x>xmin)*(x<xmax)
    col=[tuple((np.array((1,1,1))*(1-i)+np.array((1,0,0))*i).tolist()) for i in involume]

    mlab.plot3d( np.array(x), np.array(y), np.array(z))
    for i in range(len(x)):
        mlab.points3d(x[i],y[i],z[i],scale_factor=0.2, color=col[i])
elif len(sys.argv)==3 and sys.argv[1]=="clusters":
    print "Plotting an heterogeneous nucleus"

    ## File parsing
    inFile = sys.argv[2]
    if not os.path.isfile(inFile):
        print "Input file not found: {}".format(inFile)
    f = open(inFile, 'r').readlines()
    ll = [i.replace('\n','').split(',') for i in f]
    l = [{ll[0][j]: k for (j,k) in enumerate(i)} for i in ll[1:]]
    
    ## Sphere plotting
    print "Plotting {} clusters of radius {} µm".format(len(l), l[0]['r'])
    for sph in l:
        sphere = mlab.points3d(float(sph['x']), float(sph['y']), float(sph['z']),
                               scale_factor=2*float(sph['r']),
                               color=(0.7, 0.7, 0.7),
                               resolution=30,
                               opacity=1,
                               name='Nucleus')
elif len(sys.argv)>=3 and sys.argv[1] in ('csv', 'movie'):
    np.random.seed(0)
    print "Ploting from file"
    da = pd.read_csv(sys.argv[2])

    # Here take care of the splitting per frame
    nframes = 1000
    dt = 0.02
    allg = []
    empty = False
    if sys.argv[1]=='movie':
        cf = int(sys.argv[3])
        for (i,g) in da.groupby('trajectory'):
            g.t += dt*np.random.randint(nframes)-float(g.t.values[0])
            allg.append(g)
        da = pd.concat(allg)
        if np.all(np.int_(da.t/dt)!=cf):
            print "Frame {} is an empty frame".format(cf)
            das = []
        else:
            da = da[np.int_(da.t/dt)<=cf]
            intr = da.trajectory[np.int_(da.t/dt)==cf].values
            da = da[da['trajectory'].isin(intr)]
            das = [g for (i,g) in da.groupby('trajectory')]
    else:
        das = [da]    

    for da in das:
        x = np.array(da.x)
        y = np.array(da.y)
        z = np.array(da.z)

        involume = (x>xmin)*(x<xmax)
        col=[tuple((np.array((1,1,1))*(1-i)+np.array((1,0,0))*i).tolist()) for i in involume]

        mlab.plot3d( np.array(x), np.array(y), np.array(z))
        for i in range(len(col)):
            mlab.points3d(x[i],y[i],z[i],scale_factor=0.2, color=col[i])    
else:
    print "Simply plotting an empty nucleus"

## ==== Common plotting commands
sphere = mlab.points3d(0, 0, 0, scale_factor=2*nuclear_radius,
                       color=(0.67, 0.77, 0.93),
                       resolution=150,
                       opacity=0.2,
                       name='Nucleus')

detection = mlab.points3d(0, 0, 0, scale_factor=2*nuclear_radius,
                          color=(0.67, 0.1, 0.03),
                          resolution=150,
                          opacity=0.3,
                          mode="cylinder",
                          extent = [xmin,xmax,-nuclear_radius,nuclear_radius,nuclear_radius,-nuclear_radius],
                          name='Detection volume')
mlab.view(azimuth=180, elevation=170, distance=25, focalpoint=(0,0,0))
mlab.show()
fn = './movies/diffusion{:03d}.png'.format(cf)
if not os.path.isfile(fn):
    mlab.savefig(fn, size=(1000, 1000))
else:
    print "ERROR: file exists {}".format(fn)

