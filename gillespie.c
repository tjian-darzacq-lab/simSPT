// Gillepsie-related functions
// By MW, GPLv3+, Oct. 2017
#include "simSPT_structs.h"
#include "gillespie.h"
#include "distributions.h"

// Simple helper function to compute an index in the K matrix
int idx(int src, int dst, int np) {
  return np*(src-1)+(dst-1);
}

// Main function: implements a recursive Gillspie algorithm
// Basically, the function moves the particle until a threshold dt has been
//+reached. When this threshold has been reached, it computes the position at
//+time=dt, and saves the fact that we moved more than that (by saving the
//+current state and the time of jump.
void gillespie_move(float *t, float *tt, struct point *p, int *curr_pop, 
		    int *old_pop, float dt, 
		    struct diffusionPars *pops, double popsK[], int n_pops,
		    gsl_rng *prng) {
  //printf("C'est l'araignée Gillespie qui monte à a la gouttière");
  float coef;
  float newt=-1;
  float tmpt;
  int i;
  if (*t>dt) { // It's time to return
    // Compute the position at time dt
    coef = sqrt(2*pops[*old_pop].D*(dt-*tt));
    p->x += gaussrand_zig(prng)*coef;
    p->y += gaussrand_zig(prng)*coef;
    p->z += gaussrand_zig(prng)*coef;
    
    *t-= dt; // Reset t
    *tt = 0.0;
    return;
    
  } else { // Recursive move
    // Compute the position at time t
    coef = sqrt(2*pops[*old_pop].D*(*t-*tt));
    p->x += gaussrand_zig(prng)*coef;
    p->y += gaussrand_zig(prng)*coef;
    p->z += gaussrand_zig(prng)*coef;

    // Run the clocks (starting at time t)    
    *old_pop = *curr_pop;
    *tt = *t;
    for (i=1;i<=n_pops;i++) {
      if (i-1 != *old_pop) {
	tmpt = exponential(1/popsK[idx(*old_pop+1, i, n_pops)], prng);
	if ((newt==-1) | (tmpt<newt)) {
	  newt = tmpt;
	  *curr_pop = i-1;
	}
      }
    }
    *t+=newt;
    
    // Recursive move
    gillespie_move(t, tt, p, curr_pop, old_pop, dt, pops, popsK, n_pops, prng); 
  }
}

// ==== Compute equilibrium proportions
// Basically:
// - We first test if some states are absorbing: if yes, abort
// - We then compute the eignevectors of the matrix. This tells us whether the
//   associated Markov Chain is irreductible. If yes, we take the right eigenvector,
//   which corresponds to the equilibrium proportions under these asumptions.
// To perform the linear algebra operations, we rely on the GNU Scientific Library
// (GSL).
// RTFM:
// https://www.gnu.org/software/gsl/manual/html_node/Real-Nonsymmetric-Matrices.html#Real-Nonsymmetric-Matrices
// https://www.gnu.org/software/gsl/manual/html_node/Eigenvalue-and-Eigenvector-Examples.html
int get_equilibrium(double popsK[], int n_pops, struct diffusionPars *pops) {
  // ==== 1. Make sure that no state is absorbing
  float sumOut = 0;
  int i,j;
  for (i=1;i<=n_pops;i++) {
    sumOut = 0;
    for (j=1;j<=n_pops;j++) {
      if (i!=j) {
	sumOut += popsK[idx(i,j, n_pops)];
      }
    }
    popsK[idx(i,i, n_pops)]=-sumOut;
    if (sumOut==0) {
      printf("ERROR: state %d is absorbing, reparametrize your system.\n", i);
      return 1;
    }
  }  

  // ==== 2. Printing the transition matrix
  printf("==== Matrix of transition rates (Row: src. Column: dst.) ====\n");
  for (i=1;i<=n_pops;i++) {
    for (j=1;j<=n_pops;j++) {
      if (i!=j) {
	printf("%f\t",popsK[idx(i,j,n_pops)]);
      } else {
	printf("-\t");
      }
    }
    printf("\n");
  }
  printf("\n");
  
  // ==== 3. Compute equilibrium concentrations
  // First, we compute the discret time Markov chain P (data) from
  // the transition matrix Q (popsK). This involves computing:
  // P_ij = Q_ij/v_i, with v_i=-Q_ii if i!=j
  // Pii = 0
  
  double *data = malloc(n_pops*n_pops*sizeof(double)); // Matrix P
  
  for (i=1;i<=n_pops;i++) {
    for (j=1;j<=n_pops;j++) {
      if (i!=j) {
	data[idx(i,j,n_pops)]=popsK[idx(i,j, n_pops)]/-popsK[idx(i,i, n_pops)];
      } else {
	data[idx(i,j, n_pops)]=0;
      }
      //printf("%f\t", data[idx(i,j, n_pops)]);
    }
    //printf("\n");
  }

  // Compute the eigenvalue and the eigenvectors
  gsl_matrix_view m = gsl_matrix_view_array(data, n_pops, n_pops);
  gsl_vector_complex *eval = gsl_vector_complex_alloc(n_pops);
  gsl_matrix_complex *evec = gsl_matrix_complex_alloc(n_pops, n_pops);

  gsl_eigen_nonsymmv_workspace * w = gsl_eigen_nonsymmv_alloc(n_pops);
  gsl_eigen_nonsymmv(&m.matrix, eval, evec, w);
  gsl_eigen_nonsymmv_sort(eval, evec, GSL_EIGEN_SORT_ABS_DESC);
  gsl_eigen_nonsymmv_free(w);

  // Check that the dominant eigenvalue is 1
  gsl_complex eval_dom;
  bool found = false;
  for (j=0;j<n_pops;j++) {
    eval_dom = gsl_vector_complex_get(eval, j);
    if ((GSL_IMAG(eval_dom)<FLT_EPSILON) &
	(GSL_REAL(eval_dom)<1+FLT_EPSILON) &
	(GSL_REAL(eval_dom)>1-FLT_EPSILON)) {
      found = true;
      break;
    }
  }
  
  if (!found) {
    printf("The dominant eigenvalue of the system is not 1. Aborting.\n");
    printf("DEBUG: here are the eigenvalues:\n");
    for (i=0;i<n_pops;i++) {
      eval_dom = gsl_vector_complex_get (eval, i);
      printf("%f + %fi\n", GSL_REAL(eval_dom), GSL_IMAG(eval_dom));
    }
    exit(1);
  }

  // We performed the checks, it worked, so now we extract the eigenvector
  // And rescale it:
  // a_j = (pi_j/v_j)/(\sum_k (pi_k/v_k), with:
  //  - a_j the steady-state distribution
  //  - pi_j the steady_state distribution of the discrete time Markov Chain
  //  - v_k as define above
  gsl_vector_complex_view pi = gsl_matrix_complex_column(evec, j);

  double denum=0;
  gsl_complex pi_i;
  for (i=1;i<=n_pops;i++) {
    pi_i = gsl_vector_complex_get(&pi.vector, i-1);
    denum += GSL_REAL(pi_i)/popsK[idx(i,i,n_pops)];
  }
  
  for (i=1;i<=n_pops;i++) {
    pi_i = gsl_vector_complex_get(&pi.vector, i-1);
    //printf("%f - ", GSL_REAL(pi_i)/popsK[idx(i,i,n_pops)]/denum);
    pops[i-1].p = GSL_REAL(pi_i)/popsK[idx(i,i,n_pops)]/denum;
  }

  gsl_vector_complex_free(eval);
  gsl_matrix_complex_free(evec);
  free(data);  
  return 0;
}
