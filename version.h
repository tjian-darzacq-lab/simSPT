char *version = "v1.5.1";

char *version_txt = "Running simSPT version %s.\n" 
  "For any question, contact Maxime Woringer <maxime@woringer.fr>,\n"
  "or submit an issue to our bugtracker: https://gitlab.com/tjian-darzacq-lab/simSPT/issues\n\n";
