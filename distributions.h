#include <math.h>
#include <stdlib.h>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>

double exponential(double mean, gsl_rng *prng);
double gaussrand(); // Deprecated
double gaussrand_zig(gsl_rng *prng);
double power(double a, double b, double g, gsl_rng *prng);
