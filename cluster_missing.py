# -*- coding: utf-8 -*-
import numpy as np
import os

t = np.array([1,4,7,10,13,20])/1000.
stD = 0.5
strD = [0.5, 15]

stP = 0.05
strP = [0, 1]
bn = "./simulations/sim{}_{}"

## Compute the grid
nstD = int((strD[1]-strD[0])/stD)
nstD0 = int(strD[0]/stD)
nstP = int((strP[1]-strP[0])/stP)
nstP0 = int(strP[0]/stP)

p, d, tt = np.mgrid[nstP0:(nstP0+nstP), nstD0:(nstD0+nstD), :len(t)]
pp = p.flatten()*stP
d = d.flatten()*stD
tt = t[tt.flatten()]

bn="./simulations/170621_D{}_p{}_dt{}s.csv"
pattern = "-D1=0.001 -D2={} -p={} -sigma=0.035 -dt={} -n_traj=100000 -file={} -seed={}"
for i in range(len(d)):
    p = pp[i]
    D2 = d[i]
    dt = tt[i]
    bnf = bn.format(D2, p, dt)
    if not os.path.isfile(bnf):
        print pattern.format(D2, p, dt, bnf, i)
