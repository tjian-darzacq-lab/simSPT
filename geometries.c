#include "simSPT_structs.h"
#include "geometries.h"
#include "geometries_sphere.h"
#include "geometries_cube.h"

void initializeGeometry(struct geometryPars pars,
			struct geometryInit *geometry_init) {
  if (geometry_init == NULL) {
    printf("Could not allocate the geometry_init object. This smells bad. Segfault on the way :s");
  };
  geometry_init->geometry = pars.geometry;
  
  if (pars.geometry==0) { // Initialize a sphere
    //printf("initializing sphere geometry\n");
    *geometry_init = sphere_initialize_geometry(*geometry_init, pars);
  } else if (pars.geometry==1) { // Initialize a cube geometry
    //printf("initializing cube geometry\n");
    *geometry_init = cube_initialize_geometry(*geometry_init, pars);

  }
}

struct point get_random_initialization_point(struct geometryInit pars,
					     gsl_rng *prng, bool sim2d) {
  struct point p;
  if (pars.geometry == 0) {
    p = sphere_get_random_initialization_point(p, pars, prng, sim2d);
  } else {
    printf("Unsupported geometry. ABORTING");
    exit(1);
  }
  return p;
}

struct point confine_translocation(struct point pp, struct point cp,
				   struct geometryInit geometry_init,
				   gsl_rng *prng) {
  if (geometry_init.geometry==0) {
    return sphere_confine_translocation(pp, cp, geometry_init, prng);
  } else {
    printf("ERROR: Not implemented confinment\n");
    return cp;
  }
};
