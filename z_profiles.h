#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include "distributions.h"
float normal(float x, float mu, float fwhm);
float flattopnormal(float x, float fwhm, float plateau);
struct zprofileInit initialize_z_profile(struct detectionPars pars,
					 struct geometryInit geometry_init,
					 struct zprofileInit *zprofile_init);
void print_zprofile(struct detectionPars pars,
		    struct geometryInit geometry_init,
		    struct zprofileInit z_profile);
