#include <stdio.h>
#include <stdlib.h>
#include <float.h>
#include <string.h>
#include <stdbool.h>



// Function prototypes
int read_command_line(int argc, char *argv[]);

void read_command_line_simulation(int argc, char *argv[], double popsK[],
				  struct diffusionPars *pops,
				  struct simulationPars *simulation_params,
				  struct geometryPars *geometry_params,
				  struct savePars *save_params_pt,
				  float *beta,
				  int *stopCriterion, int *stopCriterionValue,
				  int *seed, int n_pops, int use_gillepsie,
				  char **clusters_file, bool *use_clusters_file);
int read_command_line_nstates(int argc, char *argv[], bool *use_gillepsie);
