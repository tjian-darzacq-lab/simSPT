// Simulation of heterogeneous nucleus.
// By MW, GPLv3+, Sept. 2017

// ==== Includes & defines
#include "simSPT_structs.h"
#include "heterogeneous.h"

#define CR 13            /* Decimal code of Carriage Return char */
#define LF 10            /* Decimal code of Line Feed char */
#define MAX_REC_LEN 1024 /* Maximum size of input buffer */

// ==== Variables
double sizeC = 0.5;  // Size of one individual cluster
int   typeC = 0;    // Type of cluster to be generated. So far, only sphere is
                    //+supported (sphere <=> typeC=0)
int   numberC = 25; // Number of clusters to be simulated
float enrichC = 10; // Relative enrichment of particles inside the
                    //cluster wrt.outside
float boundC = 0.5; // The bound fraction inside the clusters
int numbernucleiC = 1;// The number of nuclei to simulate with such settings
char *outpath = "./heterogeneous_clusters.txt"; // The file where to save all this
int nattemptsC = 500; // Number of attempts to do to add a cluster to an existing configuration, provided that it fits inside the nucleus

// Here we need to parse:
// All the options are suffixed by "C" to indicate that we are dealing with
//+clusters
//  1. The cluster size (-sizeC)
//  2. The type of clusters (only sphere for now, -typeC=sphere)
//  3. The number of clusters (-numberC)
//  4. The enrichment of particles inside clusters (-enrichC)
//  5. The bound fraction inside the clusters (-boundC)
//  6. The number of nuclei to simulate with those settings (-numbernucleiC)
//  7. The file where to save stuff (-fileC)
void read_command_line_make_heterogeneous(int argc, char *argv[]) {
  /*int i; */
  /* for (i=2;i<argc;i++) { */
  /*   if (strncmp(argv[i], "-D1=", 4)==0) {        // sizeC */
  /*     pops[0].D = atof(&(argv[i][4])); */
  /*   } else if (strncmp(argv[i], "-D2=", 4)==0) { // typeC */
  /*     pops[1].D = atof(&(argv[i][4])); */
  /*   } else if (strncmp(argv[i], "-D3=", 4)==0) { // numberC */
  /*     pops[2].D = atof(&(argv[i][4])); */
  /*     *n_pops = 3; // Then we have 3 states */
  /*   } else if (strncmp(argv[i], "-p1=", 4)==0) { // enrichC */
  /*     pops[0].p = atof(&(argv[i][4])); */
  /*   } else if (strncmp(argv[i], "-p2=", 4)==0) { // boundC */
  /*     pops[1].p = atof(&(argv[i][4])); */
  /*   } else if (strncmp(argv[i], "-p3=", 4)==0) { // fileC */
  /*     pops[2].p = atof(&(argv[i][4])); */
  /*   } else if (strncmp(argv[i], "-p=", 3)==0) { // p (2 states case) */
  /*     pops[0].p = atof(&(argv[i][3])); */
  /*     pops[1].p = 1-atof(&(argv[i][3])); */
  /*   } else if (strncmp(argv[i], "-sigma=", 7)==0) { */
  /*     simulation_params->locerror = atof(&(argv[i][7])); */
  /*   } else if (strncmp(argv[i], "-dt=", 4)==0) { */
  /*     simulation_params->dt = atof(&(argv[i][4])); */
  /*   } else if (strncmp(argv[i], "-n_traj=", 8)==0) { */
  /*     *stopCriterionValue = (int)atof(&(argv[i][8])); */
  /*     *stopCriterion = 1; */
  /*   } else if (strncmp(argv[i], "-file=", 6)==0) { */
  /*     save_params_pt->path = &(argv[i][6]); */
  /*   } else if (strncmp(argv[i], "-seed=", 6)==0) { */
  /*     *seed = (int)atof(&(argv[i][6])); */
  /*   } */
  /* }   */
  printf("Reading from command line not implemented.!\n");
}

// Print the parameters we got as input
void print_command_line_summary_make_heterogeneous(struct geometryInit geometry_init) {
  printf("==== Generating heterogeneous nuclei ====\n");
  printf("    with the following parameters:\n");
  printf("number of nuclei: %d\n", numbernucleiC);
  printf("number of clusters: %d\n", numberC);
  printf("size of one individual cluster: %f µm\n", sizeC);
  printf("relative enrichment of particles inside clusters: %f\n", enrichC);
  printf("bound fraction inside clusters: %f\n", boundC);

  // Info about the geometry
  if (geometry_init.geometry==0) {
    printf("simulation inside a sphere of radius %f µm\n", geometry_init.sphere_radius);
  } else if (geometry_init.geometry==1) {
    printf("simulation inside a cube of radius %f µm\n", geometry_init.max_x-geometry_init.min_x);
  }
  
  printf("all this will be saved to: %s\n", outpath);
}

// From https://stackoverflow.com/a/1515208
// Remove linebreak in place
void strip(char *s) {
    char *p2 = s;
    while(*s != '\0') {
        if(*s != '\t' && *s != '\n') {
            *p2++ = *s++;
        } else {
            ++s;
        }
    }
    *p2 = '\0';
}

char* get_nth_field(char *p, int n) {
  int i=0;
  strip(p);
  p = strtok (p,",");

  while (p != NULL) {
      if (i==n) {
	break;
      }
      p = strtok (NULL, " ,");      
      i++;
    }
  return p;
}

// Function that returns the number of fields in a string, based on the
//+comma ',' delimiter.
int count_number_of_fields(char const *str) {
    char const *p = str;
    int count;
    for (count = 0; ; ++count) {
        p = strstr(p, ",");
        if (!p)
            break;
        p++;
    }
    return count;
}

void HR(int iLen) {/* Print a horizontal line of iLen length                   */
  int i;

  for (i = 0; i < iLen; i++)
    printf("-");

  printf("\n");
  return;
}

void PrintLine(char *szReadLine,  long lLineCount,    long lLineLen,
               long *lLastFilePos, int  isFilePosErr)
/******************************************************************************/
/* Use:       Print detail for current line                                   */
/*                                                                            */
/* Arguments: char *szReadLine   = Read buffer containg text line             */
/*            long  lLineCount   = Current line number                        */
/*            long  lLineLen     = Current line length                        */
/*            long  lThisFilePos = Offset of start of current line            */
/*            long *lLastFilePos = Offset of end of current line              */
/*            int   isFilePosErr = True if start of current line is not       */
/*                                   1 greater than end of last line          */
/*                                                                            */
/* Return:    void                                                            */
/******************************************************************************/
{
  char *cPtr; /* Pointer to current character */

  HR(80); /* Print a separator line */
  printf("LINE %ld, Length=%#x (dec %ld)\n",
         lLineCount, (int)lLineLen, lLineLen); /* See PrintHeader() for an    */
                                               /* explanation of why the cast */
                                               /* is needed.                  */
  cPtr = szReadLine; /* Point to start of string */

  printf(" Char: ");

  /* Print the characters, including null terminator */
  for (cPtr = szReadLine; cPtr <= szReadLine + lLineLen; cPtr++)
  {
    switch (*cPtr)
    {
      case 0:                 /* Null terminator */
        printf(" \\0");
        break;

      case CR:                /* Carriage return */
        printf(" cr");
        break;

      case LF:                /* Line feed */
        printf(" lf");
        break;

      default:                /* A 'real' character */
        printf("%c", *cPtr);
        break;

    } /* end switch (*cPtr) */

  } /* end for (cPtr) */

  printf("\n");
  return;

} /* end PrintLine() */

// Function that reads from an input file and dynamically
//+instanciates a struct with all the required information,
//+including the blank fields required for downstream
//+processing.
// Should return the length of the array (that is the number of clusters)
// Note: this is greatly inspired from: http://www.mrx.net/c/source.html
int read_clusters_input_file(char *fileName, bool use_clusters_file,
			     struct cluster **clusters,
			     struct geometryPars geometry_params, struct
			     diffusionPars * pops) {
  int n_clusters;
  int n_fields;
  //bool found_all_fields=false;
  struct field {int nucleus; int cluster; double x; double y; double z; double r;
    double F_bound; double enrich; int nattempts;
  };

  struct field fields = {.nucleus = -1,
			 .cluster = -1,
			 .x = -1,
			 .y = -1,
			 .z = -1,
			 .r = -1,
			 .F_bound = -1,
			 .enrich = -1,
			 .nattempts = -1
  };
  char *current_field;
  long chksum = 1;
  int i;
  long  lFileLen;               /* Length of file */
  FILE *inputFilePtr;           /* Pointer to input file */
  char *cFile;                  /* Dynamically allocated buffer (entire file) */
  char *cThisPtr;               /* Pointer to current position in cFile */
  char  cThisLine[MAX_REC_LEN]; /* Contents of current line */
  int   isNewline;              /* Boolean indicating we've read a CR or LF */
  long  lLineCount;             /* Current line number */
  //long  lLineLen;               /* Current line length */
  //long  lStartPos;              /* Offset of start of current line */
  long  lIndex;                 /* Index into cThisLine array */
  
  // Open file (and check whether it exists
  printf("Trying to read from %s\n", fileName);
  inputFilePtr = fopen(fileName, "r");  /* Open in TEXT mode */
  if (inputFilePtr == NULL) {           /* Could not open file */
    printf("Error opening %s\n", fileName);
    exit(1);
  }

  // Get file length
  fseek(inputFilePtr, 0L, SEEK_END);     /* Position to end of file */
  lFileLen = ftell(inputFilePtr);        /* Get file length */
  rewind(inputFilePtr);                  /* Back to start of file */

  // Actually read the file
  cFile = calloc(lFileLen + 1, sizeof(char));
  if(cFile == NULL ) {
    printf("Insufficient memory to read file.\n");
    return 1;
  }
  fread(cFile, lFileLen, 1, inputFilePtr); /* Read the entire file into cFile */
  fclose(inputFilePtr);

  lLineCount  = 0L;
  cThisPtr    = cFile;              /* Point to beginning of array */
  
  // Count lines
  while (*cThisPtr) {                /* Read until reaching null char */
    isNewline = 0;
    while (*cThisPtr) {              /* Read until reaching null char */
      if (!isNewline) {              /* Haven't read a CR or LF yet */
  	if (*cThisPtr == CR || *cThisPtr == LF) /* This char IS a CR or LF */
  	  isNewline = 1;                        /* Set flag */
      } else if (*cThisPtr != CR && *cThisPtr != LF) /* Already found CR or LF */
	break;                                     /* Done with line */
      cThisPtr++;
    } /* end while (*cThisPtr) */
    ++lLineCount;                 /* Increment the line counter */
  } /* end while (cThisPtr <= cEndPtr) */
  
  // realloc the struct + instanciate the number of elements
  n_clusters = lLineCount-1;
  *clusters = realloc(*clusters, (n_clusters+1)*sizeof(struct cluster));
  
  // populate the struct by parsing the file line by line
  lLineCount  = 0L;
  cThisPtr    = cFile;              /* Point to beginning of array */

  while (*cThisPtr) {                /* Read until reaching null char */
    lIndex    = 0L;                 /* Reset counters and flags */
    isNewline = 0;

    while (*cThisPtr) {              /* Read until reaching null char */	
      if (!isNewline){              /* Haven't read a CR or LF yet */
	if (*cThisPtr == CR || *cThisPtr == LF) /* This char IS a CR or LF */
	  isNewline = 1;                        /* Set flag */
      } else if (*cThisPtr != CR && *cThisPtr != LF) /* Already found CR or LF */
	break;                                     /* Done with line */
      cThisLine[lIndex++] = *cThisPtr++; /* Add char to output and increment */
    } /* end while (*cThisPtr) */
      
    cThisLine[lIndex] = '\0';     /* Terminate the string */
    ++lLineCount;                 /* Increment the line counter */
    //lLineLen = strlen(cThisLine); /* Get length of line */
    char *strin;
    
    // Parse the file
    if (lLineCount==1) { // If this is the first line, we want to parse the header
      n_fields = count_number_of_fields(cThisLine)+1;
      //printf("%s", cThisLine); // Print header
      for (i=0;i<n_fields;i++) {
	//current_field = get_nth_field(cThisLine, i);
	strin = strdup(cThisLine);
	current_field = get_nth_field(strin, i);
	if (strcmp(current_field, "x")==0) {
	  fields.x = i;
	  chksum=chksum*2;
	} else if (strcmp(current_field, "y")==0) {
	  fields.y = i;
	  chksum=chksum*3;
	} else if (strcmp(current_field, "z")==0) {
	  fields.z = i;
	  chksum=chksum*5;
	} else if (strcmp(current_field, "r")==0) {
	  fields.r = i;
	  chksum=chksum*7;
	} else if (strcmp(current_field, "nucleus")==0) {
	  fields.nucleus = i;
	  chksum=chksum*11;
	} else if (strcmp(current_field, "cluster")==0) {
	  fields.cluster = i;
	  chksum=chksum*13;
	} else if (strcmp(current_field, "F_bound")==0) {
	  fields.F_bound = i;
	  chksum=chksum*17;
	} else if (strcmp(current_field, "enrich")==0) {
	  fields.enrich = i;
	  chksum=chksum*19;
	} else if (strcmp(current_field, "nattempts")==0) {
	  fields.nattempts = i;
	  chksum=chksum*23;
	}
	free(strin);
      }
      if (chksum != 2*3*5*7*11*13*17*19*23) { // Make sure we have all the fields
	printf("Some columns are missing or there are duplicated columns in the CSV\n");
	printf("ABORTING!\n");
	printf("%ld, %ld", chksum, (long)2*3*5*7*11*13*17*19*23);
	exit(1);
      }
    } else { // Now let's parse the file, youhou!
      strin = strdup(cThisLine);
      clusters[0][lLineCount-2].nucleus = (int)atof(get_nth_field(strin, fields.nucleus));
      free(strin);
      
      strin = strdup(cThisLine);
      clusters[0][lLineCount-2].cluster = (int)atof(get_nth_field(strin, fields.cluster));
      free(strin);

      strin = strdup(cThisLine);
      clusters[0][lLineCount-2].point.x =atof(get_nth_field(strin, fields.x));
      free(strin);
      
      strin = strdup(cThisLine);
      clusters[0][lLineCount-2].point.y =atof(get_nth_field(strin, fields.y));
      free(strin);
      
      strin = strdup(cThisLine);
      clusters[0][lLineCount-2].point.z =atof(get_nth_field(strin, fields.z));
      free(strin);
      
      strin = strdup(cThisLine);
      clusters[0][lLineCount-2].r = atof(get_nth_field(strin, fields.r));
      free(strin);
      
      strin = strdup(cThisLine);
      clusters[0][lLineCount-2].enrich = atof(get_nth_field(strin, fields.enrich));
      free(strin);
      
      strin = strdup(cThisLine);
      clusters[0][lLineCount-2].nattempts = (int)atof(get_nth_field(strin, fields.nattempts));
      free(strin);
      
      strin = strdup(cThisLine);
      clusters[0][lLineCount-2].pops[0].p = atof(get_nth_field(strin, fields.F_bound));
      free(strin);
      
      strin = strdup(cThisLine);
      clusters[0][lLineCount-2].pops[1].p = 1-clusters[0][lLineCount-2].pops[0].p;
      free(strin);

      clusters[0][lLineCount-2].area = 4/3*M_PI*pow(clusters[0][lLineCount-2].r, 3);
    }
  } /* end while (cThisPtr <= cEndPtr) */
  free(cFile);
  
  /* for (i=0;i<(n_clusters+1);i++) { */
  /*   printf("%f, %f\n", clusters[0][i].area, clusters[0][i].r); */
  /* } */

  // ==== Initialize the structures
  if (geometry_params.geometry != 0) {
    printf("Clusters simulation only handles sphere geometry so far");
    exit(1);
  }
  
  double sumprob = 0;
  (*clusters)[n_clusters].r = geometry_params.radius;
  (*clusters)[n_clusters].area = 4/3*M_PI*pow(geometry_params.radius, 3);
  (*clusters)[n_clusters].prob = (*clusters)[n_clusters].area;
  sumprob += (*clusters)[n_clusters].prob;  

  for (i=0;i<n_clusters;i++) {
    (*clusters)[n_clusters].area -= (*clusters)[i].area;
    (*clusters)[i].prob = (*clusters)[i].area*(*clusters)[i].enrich;
    (*clusters)[i].pops[0].D = pops[0].D;
    (*clusters)[i].pops[0].beta = pops[0].beta;
    (*clusters)[i].pops[0].betaUnit = pops[0].betaUnit;
    (*clusters)[i].pops[1].D = pops[1].D;
    (*clusters)[i].pops[1].beta = pops[1].beta;
    (*clusters)[i].pops[1].betaUnit = pops[1].betaUnit;

    sumprob += (*clusters)[i].prob;
  }

  (*clusters)[n_clusters].pops[0] = pops[0]; // WARNING, HERE ONLY 2 STATES
  (*clusters)[n_clusters].pops[1] = pops[1];

  for (i=0; i<(n_clusters+1); i++) {
    (*clusters)[i].prob /= sumprob;
  }

  return n_clusters;
}


// Test if two (sphere) clusters intersect. Returns 1 if yes
int cluster_intersect(struct point p1, struct point p2, double rC) {
  if ((pow(p1.x-p2.x,2)+pow(p1.y-p2.y,2)+pow(p1.z-p2.z,2))<pow(2*rC,2)) {
    return 1;
  } else {
    return 0;
  }
}

// Test if the cluster lies totally within the nucleus.
// So far, only the 'sphere' and 'cube' geometries are supported.
// Return 1 if yes
int cluster_in_geometry(struct point cC, struct geometryInit gi, double sizeC) {
  if (gi.geometry == 0) {   // Sphere geometry
    double r = sqrt(pow(cC.x,2)+pow(cC.y,2)+pow(cC.z,2))+sizeC;
    if (r > gi.sphere_radius) {
      return 0;
    } else {
      return 1;
    }
  } else if (gi.geometry == 1) {   // Cube geometry
    if ((cC.x-sizeC < gi.min_x) || (cC.x+sizeC > gi.max_x) ||
	(cC.y-sizeC < gi.min_y) || (cC.y+sizeC > gi.max_y) ||
	(cC.z-sizeC < gi.min_z) || (cC.z+sizeC > gi.max_z)){
      return 0;
    } else {     
      return 1;
    }
  } else { // Unsupported geometry
    printf("Unsupported geometry, index: %d", gi.geometry);
    exit(1);
  }
}

// Generate the heterogeneous nucleus
int make_heterogeneous_nucleus(struct geometryInit geometry_init, gsl_rng *prng) {
  // malloc the structure for one nuclei. It is uninitialized until we realloc it
  // returns the number of clusters
  struct cluster *clusters = malloc(numberC*sizeof(struct cluster)); 
  int i;
  int j;
  int nucl;
  int done;
  int collision;
  float vol_nucleus;
  //float vol_clusters;
  FILE *fout;
  printf("Generating a heterogeneous nucleus\n");

  // ==== Sanity checks
  // First check that there is a chance that we can pack stuff in this nucleus:
  // Compare the volume of the nucleus with the volume of the clusters + 34%
  if (geometry_init.geometry==0) {
    vol_nucleus = 4/3.*M_PI*pow(geometry_init.sphere_radius,3);
  } else if (geometry_init.geometry==1) {
    vol_nucleus = pow(geometry_init.max_x-geometry_init.min_x,3);
  } else {
    printf("Unrecognized geometry. ABORTING.\n");
    exit(1);
  }
  if (1.5*numberC*4/3.*M_PI*pow(sizeC,3)>vol_nucleus) {
    printf("It is mathematically impossible to pack that many ");
    printf("non-overlapping clusters inside such a small nucleus\n");
    printf("ABORTING!\n");
    exit(1);
  }

  // ==== Initialize the output file
  fout = fopen(outpath, "w");
  fprintf(fout, "nucleus,cluster,x,y,z,r,F_bound,enrich,nattempts\n"); // header
  fclose(fout);

  // ==== Generate the nuclei
  for (nucl = 0;nucl < numbernucleiC;nucl++) { // For all the nuclei

    // ==== Reset struct
    for (i=0;i<numberC;i++) { // Initialize the structure to empty stuff.
      clusters[i].nattempts = 0;
    }

    // ==== Generate clusters
    for (i=0;i<numberC;i++) { // Iterate over all possible clusters
      done = 0;
      while (done == 0) {
	// Draw a point so that the cluster is inside the nucleus
	clusters[i].point = get_random_initialization_point(geometry_init, prng, false);
	printf("BUG: Not ready for 2D sim");
	while (cluster_in_geometry(clusters[i].point, geometry_init, sizeC)==0) {
	  clusters[i].point = get_random_initialization_point(geometry_init, prng, false);
	  printf("BUG: Not ready for 2D sim");
	}
	//printf("%f\t%f\t%f\n", clusters[i].point.x, clusters[i].point.y, clusters[i].point.z);
	collision = 0;
	for (j=0;j<i;j++) { // Check if no positional conflict
	  if (cluster_intersect(clusters[j].point, clusters[i].point, sizeC)==1) {
	  collision=1;
	  break;
	  }
	}
	if (collision==0) { // Set to done if we are all good.
	  done=1;
	  clusters[i].r = sizeC;
	  clusters[i].boundC = boundC;
	  clusters[i].enrichC = enrichC;
	}

	clusters[i].nattempts++;
	if (clusters[i].nattempts==nattemptsC) {
	  clusters[i].nattempts = -1; // -1 means that we failed
	  done = 1;
	}
      }
    }

    // ==== Save - note that the header has been saved to file earlier
    // Remember to update it when changing the output format.
    fout = fopen(outpath, "a");
    for (i=0;i<numberC;i++) {
      fprintf(fout, "%d,%d,%f,%f,%f,%f,%f,%f,%d\n",nucl,i+1,
	      clusters[i].point.x, clusters[i].point.y, clusters[i].point.z,
	      clusters[i].r, clusters[i].boundC, clusters[i].enrichC,
	      clusters[i].nattempts);
    }
    fclose(fout);

    // ==== free
    free(clusters);
  }
  return numberC;
}



// Return true if the point is in the right cluster
bool point_in_cluster(point p, struct cluster **clusters_pt, int curr_cluster, int n_clusters) {
  struct point center = (*clusters_pt)[curr_cluster].point;
  double r = (*clusters_pt)[curr_cluster].r;

  if (curr_cluster == n_clusters) {
    return curr_cluster==point_in_which_cluster(p, clusters_pt, n_clusters);
  }
  if (pow(p.x-center.x, 2)+pow(p.y-center.y, 2)+pow(p.z-center.z, 2) < pow(r,2)) {
    return true;
  } else {
    return false;
  }
}

// Return in which cluster is the current point
int point_in_which_cluster(point p, struct cluster **clusters_pt,
			   int n_clusters) {
  int i=0;
  struct point center;
  double r;
  
  for (i=0;i<n_clusters;i++) {
    center = (*clusters_pt)[i].point;
    r = (*clusters_pt)[i].r;
    if (pow(p.x-center.x, 2)+pow(p.y-center.y, 2)+pow(p.z-center.z, 2)<pow(r,2)) {
      return i;
    } else {
      i++;
    }
  }
  return n_clusters;
}
