// ==== Correlator  ====
// By Patrick Ferrand (unknown date, unknown license)
// remastered by MW, 2018
//
// The Correlator calculates the intensity autocorrelation function from a list
// of positions in a file (Filename is declared at line 40) and writes it in
// correl.out
//
// Format of Filename: X0	Y0
//					X1	Y15
//					.......
//						
//Format of correl.out: (time delay) (autocorrelation function) (standard deviation)

#include <math.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h> 

#define EPS 1.2e-7
#define IA 16807
#define IM 2147483647
#define AM (1.0/IM)
#define IQ 127773
#define IR 2836
#define NTAB 32
#define NDIV (1+(IM-1)/NTAB)

#define RNMX (1.0-EPS)
#define PI 3.141592654

#define wxy 2.5


#define xf  0
#define yf  0
#define lambda 30
#define N_channels 8
#define N_correlators 18
#define delta_t 0.2


// ==== variables globales ====

//// File to read /////////////////////////////////////////////////////////////////////////////////////
char *Filename="intensite";


double crl[144];
long d=-3;
long *iidum=&d;


double dCorrelation[(N_correlators)*(N_channels-1)], dSigma[(N_correlators)*(N_channels-1)];
unsigned long int Channel[N_channels][N_correlators], Channel_[N_channels][N_correlators], Delayed_monitor[N_channels][N_correlators], T_[N_channels][N_correlators];
unsigned long int Direct_monitor[N_correlators],Ch_0Delay[N_correlators];
long double Correl[N_channels][N_correlators],Sigma2[N_channels][N_correlators];
unsigned long int N_CHsum[N_channels][N_correlators] ;
double temps[145];


FILE *stream;
FILE *stream2;
FILE *stream3;
FILE *moyen;



int DoCorrelation();

/////////////////////////////////////////////////////////////////////////////////////
//////////////////////// fonctions pour la fonction erreur //////////////////////////
/////////////////////////////////////////////////////////////////////////////////////


#define NR_END 1
#define FREE_ARG char*
   
void nrerror(char error_text[])
/* Numerical Recipes standard error handler */
{
	fprintf(stderr,"Numerical Recipes run-time error...\n");
	fprintf(stderr,"%s\n",error_text);
	fprintf(stderr,"...now exiting to system...\n");
	exit(1);
}
   
float *vector(long nl, long nh)
/* allocate a float vector with subscript range v[nl..nh] */
{
	float *v;
   
	v=(float *)malloc((size_t) ((nh-nl+1+NR_END)*sizeof(float)));
	if (!v) nrerror("allocation failure in vector()");
	return v-nl+NR_END;
}
   
int *ivector(long nl, long nh)

/* allocate an int vector with subscript range v[nl..nh] */
{
	int *v;
   
	v=(int *)malloc((size_t) ((nh-nl+1+NR_END)*sizeof(int)));
	if (!v) nrerror("allocation failure in ivector()");
	return v-nl+NR_END;
}
   
unsigned char *cvector(long nl, long nh)
/* allocate an unsigned char vector with subscript range v[nl..nh] */
{
	unsigned char *v;
   
	v=(unsigned char *)malloc((size_t) ((nh-nl+1+NR_END)*sizeof(unsigned char)));
	if (!v) nrerror("allocation failure in cvector()");
	return v-nl+NR_END;
}
   
unsigned long *lvector(long nl, long nh)
/* allocate an unsigned long vector with subscript range v[nl..nh] */
{
	unsigned long *v;
   
	v=(unsigned long *)malloc((size_t) ((nh-nl+1+NR_END)*sizeof(long)));
	if (!v) nrerror("allocation failure in lvector()");

	return v-nl+NR_END;
}
   
double *dvector(long nl, long nh)
/* allocate a double vector with subscript range v[nl..nh] */
{
	double *v;
   
	v=(double *)malloc((size_t) ((nh-nl+1+NR_END)*sizeof(double)));
	if (!v) nrerror("allocation failure in dvector()");
	return v-nl+NR_END;
}
   
float **matrix(long nrl, long nrh, long ncl, long nch)
/* allocate a float matrix with subscript range m[nrl..nrh][ncl..nch] */
{
	long i, nrow=nrh-nrl+1,ncol=nch-ncl+1;
	float **m;
   
	/* allocate pointers to rows */
	m=(float **) malloc((size_t)((nrow+NR_END)*sizeof(float*)));
	if (!m) nrerror("allocation failure 1 in matrix()");
	m += NR_END;
	m -= nrl;
   
	/* allocate rows and set pointers to them */
	m[nrl]=(float *) malloc((size_t)((nrow*ncol+NR_END)*sizeof(float)));
	if (!m[nrl]) nrerror("allocation failure 2 in matrix()");
	m[nrl] += NR_END;

	m[nrl] -= ncl;
   
	for(i=nrl+1;i<=nrh;i++) m[i]=m[i-1]+ncol;
   
	/* return pointer to array of pointers to rows */
	return m;
}
   
double **dmatrix(long nrl, long nrh, long ncl, long nch)
/* allocate a double matrix with subscript range m[nrl..nrh][ncl..nch] */
{
	long i, nrow=nrh-nrl+1,ncol=nch-ncl+1;
	double **m;
   
	/* allocate pointers to rows */
	m=(double **) malloc((size_t)((nrow+NR_END)*sizeof(double*)));
	if (!m) nrerror("allocation failure 1 in matrix()");
	m += NR_END;
	m -= nrl;
   
	/* allocate rows and set pointers to them */
	m[nrl]=(double *) malloc((size_t)((nrow*ncol+NR_END)*sizeof(double)));
	if (!m[nrl]) nrerror("allocation failure 2 in matrix()");
	m[nrl] += NR_END;
	m[nrl] -= ncl;
   
	for(i=nrl+1;i<=nrh;i++) m[i]=m[i-1]+ncol;
   
	/* return pointer to array of pointers to rows */
	return m;
}
   
int **imatrix(long nrl, long nrh, long ncl, long nch)
/* allocate a int matrix with subscript range m[nrl..nrh][ncl..nch] */

{
	long i, nrow=nrh-nrl+1,ncol=nch-ncl+1;
	int **m;
   
	/* allocate pointers to rows */
	m=(int **) malloc((size_t)((nrow+NR_END)*sizeof(int*)));
	if (!m) nrerror("allocation failure 1 in matrix()");
	m += NR_END;
	m -= nrl;
   
   
	/* allocate rows and set pointers to them */
	m[nrl]=(int *) malloc((size_t)((nrow*ncol+NR_END)*sizeof(int)));
	if (!m[nrl]) nrerror("allocation failure 2 in matrix()");
	m[nrl] += NR_END;
	m[nrl] -= ncl;
   
	for(i=nrl+1;i<=nrh;i++) m[i]=m[i-1]+ncol;
   
	/* return pointer to array of pointers to rows */
	return m;
}
   
double **submatrix(double **a, long oldrl, long oldrh, long oldcl, long oldch,
	long newrl, long newcl)
/* point a submatrix [newrl..][newcl..] to a[oldrl..oldrh][oldcl..oldch] */
{
	long i,j,nrow=oldrh-oldrl+1,ncol=oldcl-newcl;

	double **m;
   
	/* allocate array of pointers to rows */
	m=(double **) malloc((size_t) ((nrow+NR_END)*sizeof(double*)));
	if (!m) nrerror("allocation failure in submatrix()");
	m += NR_END;
	m -= newrl;
   
	/* set pointers to rows */
	for(i=oldrl,j=newrl;i<=oldrh;i++,j++) m[j]=a[i]+ncol;
   
	/* return pointer to array of pointers to rows */
	return m;
}
   
double **convert_matrix(double *a, long nrl, long nrh, long ncl, long nch)
/* allocate a double matrix m[nrl..nrh][ncl..nch] that points to the matrix
declared in the standard C manner as a[nrow][ncol], where nrow=nrh-nrl+1
and ncol=nch-ncl+1. The routine should be called with the address
&a[0][0] as the first argument. */
{
	long i,j,nrow=nrh-nrl+1,ncol=nch-ncl+1;

	double **m;
   
	/* allocate pointers to rows */
	m=(double **) malloc((size_t) ((nrow+NR_END)*sizeof(double*)));
	if (!m) nrerror("allocation failure in convert_matrix()");
	m += NR_END;
	m -= nrl;
   
	/* set pointers to rows */
	m[nrl]=a-ncl;
	for(i=1,j=nrl+1;i<nrow;i++,j++) m[j]=m[j-1]+ncol;
	/* return pointer to array of pointers to rows */
	return m;
}
   
double ***f3tensor(long nrl, long nrh, long ncl, long nch, long ndl, long ndh)
/* allocate a double 3tensor with range t[nrl..nrh][ncl..nch][ndl..ndh] */
{
	long i,j,nrow=nrh-nrl+1,ncol=nch-ncl+1,ndep=ndh-ndl+1;
	double ***t;
   
	/* allocate pointers to pointers to rows */
	t=(double ***) malloc((size_t)((nrow+NR_END)*sizeof(double**)));

	if (!t) nrerror("allocation failure 1 in f3tensor()");
	t += NR_END;
	t -= nrl;
   
	/* allocate pointers to rows and set pointers to them */
	t[nrl]=(double **) malloc((size_t)((nrow*ncol+NR_END)*sizeof(double*)));
	if (!t[nrl]) nrerror("allocation failure 2 in f3tensor()");
	t[nrl] += NR_END;
	t[nrl] -= ncl;
   
	/* allocate rows and set pointers to them */
	t[nrl][ncl]=(double *) malloc((size_t)((nrow*ncol*ndep+NR_END)*sizeof(double)));
	if (!t[nrl][ncl]) nrerror("allocation failure 3 in f3tensor()");
	t[nrl][ncl] += NR_END;
	t[nrl][ncl] -= ndl;
   
	for(j=ncl+1;j<=nch;j++) t[nrl][j]=t[nrl][j-1]+ndep;
	for(i=nrl+1;i<=nrh;i++) {

		t[i]=t[i-1]+ncol;
		t[i][ncl]=t[i-1][ncl]+ncol*ndep;
		for(j=ncl+1;j<=nch;j++) t[i][j]=t[i][j-1]+ndep;
	}
   
	/* return pointer to array of pointers to rows */
	return t;
}
   
void free_vector(float *v, long nl, long nh)
/* free a float vector allocated with vector() */
{
	free((FREE_ARG) (v+nl-NR_END));
}
   
void free_ivector(int *v, long nl, long nh)
/* free an int vector allocated with ivector() */
{
	free((FREE_ARG) (v+nl-NR_END));
}
   
void free_cvector(unsigned char *v, long nl, long nh)
/* free an unsigned char vector allocated with cvector() */
{
	free((FREE_ARG) (v+nl-NR_END));
}
   
void free_lvector(unsigned long *v, long nl, long nh)
/* free an unsigned long vector allocated with lvector() */
{
	free((FREE_ARG) (v+nl-NR_END));
}
   
void free_dvector(double *v, long nl, long nh)
/* free a double vector allocated with dvector() */
{
	free((FREE_ARG) (v+nl-NR_END));
}
   

void free_matrix(float **m, long nrl, long nrh, long ncl, long nch)
/* free a float matrix allocated by matrix() */
{
	free((FREE_ARG) (m[nrl]+ncl-NR_END));
	free((FREE_ARG) (m+nrl-NR_END));
}
   
void free_dmatrix(double **m, long nrl, long nrh, long ncl, long nch)
/* free a double matrix allocated by dmatrix() */
{
	free((FREE_ARG) (m[nrl]+ncl-NR_END));
	free((FREE_ARG) (m+nrl-NR_END));
}
   
void free_imatrix(int **m, long nrl, long nrh, long ncl, long nch)
/* free an int matrix allocated by imatrix() */
{
	free((FREE_ARG) (m[nrl]+ncl-NR_END));
	free((FREE_ARG) (m+nrl-NR_END));
}
   
void free_submatrix(double **b, long nrl, long nrh, long ncl, long nch)
/* free a submatrix allocated by submatrix() */
{
	free((FREE_ARG) (b+nrl-NR_END));
}
   
void free_convert_matrix(float **b, long nrl, long nrh, long ncl, long nch)
/* free a matrix allocated by convert_matrix() */
{
	free((FREE_ARG) (b+nrl-NR_END));
}
   
void free_f3tensor(double ***t, long nrl, long nrh, long ncl, long nch,
	long ndl, long ndh)
/* free a double f3tensor allocated by f3tensor() */
{
	free((FREE_ARG) (t[nrl][ncl]+ndl-NR_END));
	free((FREE_ARG) (t[nrl]+ncl-NR_END));

	free((FREE_ARG) (t+nrl-NR_END));
}



//gammln.c------------------------------------------



   
double gammln(double xx)
{
	double x,y,tmp,ser;
	static double cof[6]={76.18009172947146,-86.50532032941677,
		24.01409824083091,-1.231739572450155,
		0.1208650973866179e-2,-0.5395239384953e-5};
	int j;
   
	y=x=xx;
	tmp=x+5.5;
	tmp -= (x+0.5)*log(tmp);
	ser=1.000000000190015;
	for (j=0;j<=5;j++) ser += cof[j]/++y;
	return -tmp+log(2.5066282746310005*ser/x);
}



//gcf.c-----------------------------------


#define ITMAX 100
#define EPS2 3.0e-7
#define FPMIN 1.0e-30
   
void gcf(double *gammcf, double a, double x, double *gln)
{
	double gammln(double xx);
	void nrerror(char error_text[]);
	int i;
	double an,b,c,d,del,h;
   
	*gln=gammln(a);
	b=x+1.0-a;
	c=1.0/FPMIN;
	d=1.0/b;
	h=d;
	for (i=1;i<=ITMAX;i++) {
		an = -i*(i-a);
		b += 2.0;
		d=an*d+b;
		if (fabs(d) < FPMIN) d=FPMIN;
		c=b+an/c;
		if (fabs(c) < FPMIN) c=FPMIN;
		d=1.0/d;
		del=d*c;
		h *= del;
		if (fabs(del-1.0) < EPS2) break;
	}
	if (i > ITMAX) nrerror("a too large, ITMAX too small in gcf");
	*gammcf=exp(-x+a*log(x)-(*gln))*h;
}

//gser.c-------------------------------------------------------------


   
void gser(double *gamser, double a, double x, double *gln)
{
	double gammln(double xx);
	void nrerror(char error_text[]);
	int n;
	double sum,del,ap;
   
	*gln=gammln(a);
	if (x <= 0.0) {
		if (x < 0.0) nrerror("x less than 0 in routine gser");
		*gamser=0.0;
		return;
	} else {
		ap=a;
		del=sum=1.0/a;
		for (n=1;n<=ITMAX;n++) {
			++ap;
			del *= x/ap;
			sum += del;
			if (fabs(del) < fabs(sum)*EPS) {
				*gamser=sum*exp(-x+a*log(x)-(*gln));
				return;
			}
		}
		nrerror("a too large, ITMAX too small in routine gser");

		return;
	}
}



//gammp.c---------------------------------------------------


double gammp(double a, double x)
{
	void gcf(double *gammcf, double a, double x, double *gln);
	void gser(double *gamser, double a, double x, double *gln);
	void nrerror(char error_text[]);
	double gamser,gammcf,gln;
   
	if (x < 0.0 || a <= 0.0) nrerror("Invalid arguments in routine gammp");
	if (x < (a+1.0)) {
		gser(&gamser,a,x,&gln);
		return gamser;
	} else {
		gcf(&gammcf,a,x,&gln);
		return 1.0-gammcf;
	}
}


//erff.c-----------------------------------------------------


double erff(double x)
{
	double gammp(double a, double x);
   
	return x < 0.0 ? -gammp(0.5,x*x) : gammp(0.5,x*x);
}

///////////////////////////////////////////////////////////////////
///////////////// fin du chargement des librairies ////////////////
///////////////////////////////////////////////////////////////////



//--------------------------fonction ran1------------------------

double ran1(long *idum)
{
	int j;
	long k;
	static long iy=0;
	static long iv[NTAB];
	double temp;
   
	if (*idum <= 0 || !iy) {
		if (-(*idum) < 1) *idum=1;
		else *idum = -(*idum);
		for (j=NTAB+7;j>=0;j--) {
			k=(*idum)/IQ;
			*idum=IA*(*idum-k*IQ)-IR*k;
			if (*idum < 0) *idum += IM;
			if (j < NTAB) iv[j] = *idum;
		}
		iy=iv[0];
	}
	k=(*idum)/IQ;
	*idum=IA*(*idum-k*IQ)-IR*k;

	if (*idum < 0) *idum += IM;
	j=iy/NDIV;
	iy=iv[j];
	iv[j] = *idum;
	if ((temp=AM*iy) > RNMX) return RNMX;
	else return temp;
}


//---------------------------fonction gasdev------------------------------
// gasdev renvoie un nb distribue selon une gaussienne de moyenne nulle d'ecart type 1
double gasdev(long *idum)
{
  
  static int iset=0;
  static double gset;
  double fac,rsq,v1,v2;

  if(iset==0)
    {
      do
	{
	  v1=2.0*ran1(idum)-1.0;
	  v2=2.0*ran1(idum)-1.0;
	  rsq=v1*v1+v2*v2;
	}
      while((rsq>=1.0)||(rsq ==0.0));
      
      fac=sqrt(-2.0*log(rsq)/rsq);
      gset=v1*fac;
      iset=1;
      return v2*fac;
    } 
  else
    {
      iset=0;
      return gset;
    }
}


//-------------------fonction poidev--------------
double poidev(double xm, long *idum)
{
	double gammln(double xx);
	double ran1(long *idum);
	static double sq,alxm,g,oldm=(-1.0);
	double em,t,y;
   
	if (xm < 12.0) {
		if (xm != oldm) {
			oldm=xm;
			g=exp(-xm);
		}
		em = -1;
		t=1.0;
		do {
			++em;
			t *= ran1(idum);
		} while (t > g);
	} else {
		if (xm != oldm) {
			oldm=xm;
			sq=sqrt(2.0*xm);
			alxm=log(xm);
			g=xm*alxm-gammln(xm+1.0);
		}
		do {
			do {
				y=tan(PI*ran1(idum));

				em=sq*y+xm;
			} while (em < 0.0);
			em=floor(em);
			t=0.9*(1.0+y*y)*exp(em*alxm-gammln(em+1.0)-g);
		} while (ran1(idum) > t);
	}
	return em;
}


//------------------fonction de correlation-----------------------



void initialisation()
{
	int  k, l;

	for(k = 0; k<= N_channels-1; k++) {
  		for(l = 0; l <= N_correlators-1; l++) {

			N_CHsum[k][l]=0;
			Channel[k][l]=0;
			Channel_[k][l]=0;
			Delayed_monitor[k][l]=0; 
			T_[k][l]=0;
		}
	} 


	
  		for(l = 0; l <= N_correlators-1; l++) {
			Direct_monitor[l]=0;
			Ch_0Delay[l]=0;
		}

	for(k = 0; k<= N_channels-1; k++) {
  		for(l = 0; l <= N_correlators-1; l++) {
			Correl[k][l]=0;
			Sigma2[k][l]=0;
		}
	} 

	for(k=0;k<=(N_correlators)*(N_channels-1);k++){
		dCorrelation[k]=0;
		dSigma[k]=0;
	}
	





}

	//////////////////
////////////////////Normalisation/////////////////
	
void normalisation(long NPTS)
{

	int k, l;


		/////////////////Number of times that a channel is updated /////////////////////
	for(k = 0; k<= N_channels-1; k++)
	{
			N_CHsum[k][0]=NPTS-k;
			N_CHsum[k][1]=NPTS-k-(N_channels);
				if (N_CHsum[k][1]<=0){
					N_CHsum[k][1]=0;
				}
	}

	for (l = 2; l <= N_correlators-1; l++) 
	{
  		for(k = 0; k<= N_channels-1; k++)
		{
			N_CHsum[k][l]=N_CHsum[N_channels-1][l-1]/2-k;
			if (N_CHsum[k][l]<=0)
			{
			N_CHsum[k][l]=0;
			}
		}
	}

		////////////////////normalisation////////////////:

	for(k = 0; k <= N_channels-1; k++)
	{
		for (l=0;l<=N_correlators-1;l++)
		{
		Correl[k][l]*=(double) N_CHsum[k][l]/Direct_monitor[l]/Delayed_monitor[k][l];
		Sigma2[k][l]=(double)Sigma2[k][l]*N_CHsum[k][l]/Direct_monitor[l]/Delayed_monitor[k][l]*N_CHsum[k][l]/Direct_monitor[l]/Delayed_monitor[k][l];
		Sigma2[k][l]-=pow(Correl[k][l],2)/((double)N_CHsum[k][l]);
		}
	}


  for(l=0;l<=N_correlators-1;l++){
		for(k=0;k<=N_channels-1;k++){
		dCorrelation[k+8*l]=Correl[k][l];
		dSigma[k+8*l]=Sigma2[k][l];
		}
	}

	
 
 for(l = 0; l<= N_channels*N_correlators; l++) {crl[l]=dCorrelation[l];} 


}


static int
DoCorrelation(long i, double n_photons)
{
	
	
	int j, k, l, m, n;

	// New measurement stored in the first channel	
	Channel[0][0]=n_photons;			

	
	//   Delayed_monitor   Sum of all counts that pass trough a channel
	for(k = 0; k <= N_channels-1; k++) 
	{
		Delayed_monitor[k][0]+=Channel[k][0];
		Delayed_monitor[k][1]+=Channel[k][1];
	}
				
	//   Direct monitor : sum of all counts that pass trough the O-delay time channel
	Direct_monitor[0]+=Channel[0][0];
	Direct_monitor[1]+=Channel[0][0];

	Ch_0Delay[0]=Channel[0][0];
	Ch_0Delay[1]=Channel[0][0];


	
	//Correlation : two first correlators
	for(k = 0; k <= N_channels-1; k++) 
	{
		Correl[k][0]+=(double)Channel[k][0]*Channel[0][0];
		Correl[k][1]+=(double)Channel[k][1]*Channel[0][0];
		Sigma2[k][0]+=pow((double) Channel[k][0]*Channel[0][0],2);
		Sigma2[k][1]+=pow((double) Channel[k][1]*Channel[0][0],2);
	}			



	for (l=2;l<=N_correlators-1;l++) 
	{
		j=(int)pow(2,(l-1));
		if (i%j==0)  
		{
			for(k = 0; k <= N_channels-1; k++) 
				{
					Delayed_monitor[k][l]+=Channel[k][l];
				}
			


			if(l<=5)
			{
				Ch_0Delay[l]=0;
					n=-1;
					for(k = 0; k <= j-1; k++)
					{
						m=abs(k%N_channels);
						if (m==0){n+=1;};
						Direct_monitor[l]+=Channel[m][n];
						Ch_0Delay[l]+=Channel[m][n];
					}
			}

	
			if(l>5)
			{
				Ch_0Delay[l]=0;
					n=-1;
					for(k = 0; k < N_channels*(l-3); k++)
					{
						m=abs(k%N_channels);
						if (m==0){n+=1;};
						Direct_monitor[l]+=Channel[m][n];
						Ch_0Delay[l]+=Channel[m][n];
					}
			}

				for (k=0;k<=N_channels-1;k++){
					Correl[k][l]+=(double)Channel[k][l]*Ch_0Delay[l];
					Sigma2[k][l]+=pow((double) Channel[k][l]*Ch_0Delay[l],2);
		
				}
		
		}
			
	}




// all channels of the two first correlators are shifted to the right

	
	for (l=N_correlators-1;l>=2;l--) 
	{
			j=(int)pow(2,(l-1));


			if ((i)%j==0)
			{	
				

				//  all channels are shifted to the right
					for(k = 1; k <= N_channels-1; k++) 
					{
						Channel_[k][l]=Channel[k-1][l];
					}
					// first channel of correlator l is evaluated from previous correlator
				
					Channel[0][l]=Channel[N_channels-1][l-1]+Channel[N_channels-2][l-1];


					for(k = 1; k <= N_channels-1; k++) 
					{
						Channel[k][l]=Channel_[k][l];
					}

			
			}
	}

	

				for(k = 1; k <= N_channels-1; k++) {
					Channel_[k][0]=Channel[k-1][0];
					Channel_[k][1]=Channel[k-1][1];
				}
				Channel[0][1]=Channel[N_channels-1][0];


				for(k = 1; k <= N_channels-1; k++) {
					Channel[k][0]=Channel_[k][0];
					Channel[k][1]=Channel_[k][1];
				}


return i;

}

//---------------------------- tableau temps -------------------------------------///
void array_timedelay()
{
	int s, j, i1, i2;

	for(s=0;s<19;s++)
	{
		temps[s]=delta_t*s*1.;
	}

	for(j=2;j<18;j++)
	{
		for(s=1;s<9;s++)
		{
			i2=18+(j-2)*8;
			i1=i2+s;
		

		temps[i1]=1.*delta_t*s*pow(2,j-1)+temps[i2];

		}
	}



}

//------------------main-----------------------------////////////////



main()
{

	/*float vect_x, vect_y;*/
	double n_photons;
	double nmoyen;
	double intensity1;

	double ini,t_apparent,N_apparent;
	int ent,s;
	
	long i=1, NPTS;
	int l;
	int time;
	array_timedelay();
	initialisation();
  
	stream=fopen("essai.txt","r");
	printf("fichier: \t",stream);
	//stream3=fopen("testint0.dat","w+");
    fseek(stream, 0L, SEEK_SET);

	while( !feof( stream ))
	{	
		fscanf(stream,"%f\t %d\n",&time, &intensity1);
		printf("%f\t %d\n",time, intensity1);
		//printf("%i\t %lf\t %lf\t %lf\t %lf\t %lf\t %lf\t %lf\t %lf\t %lf\n",time, intensity1,intensity2,intensity3,
	    //intensity4,intensity5,intensity6,intensity7,intensity8,intensity9,intensity10);
		//system("PAUSE");
			//n_photons=poidev(intensity1,iidum);
			n_photons=intensity1;
			//printf("%lf\t %lf\n",time, intensity1);
			DoCorrelation(i, n_photons);
			//fprintf(stream3,"int= %lf\t n_phot= %lf\t iidum=%d\t iidumpoi=%lf\n",intensity1,n_photons,iidum,&iidum);
			i++;
	}
	fclose(stream);
	//fclose(stream3);
	
	NPTS=i;

	normalisation(NPTS);


    
	//valeur du temps de diffusion apparent
	//la valeur a l'origine est souvent faussee
    ini=(crl[1]-1)/2.;
	s=1;
	while((s<145)&&(crl[s]>(ini+1)))
	{
		s++;
	}

	if(s==1){
		ent=1;
		t_apparent=temps[1];
	}	
	else
	{
		ent=s;
		t_apparent=temps[s-1]+(temps[s]-temps[s-1])*(crl[s-1]-ini-1)/(crl[s-1]-crl[s]);
	
	}

  

	////////////////// Write correlation data in correl.out///////////////// 
	stream2=fopen("correlw.out","w+");


	for(l = 0; l< N_channels*N_correlators; l++) {
  		
	 crl[l]=dCorrelation[l];
	 printf(" %lf\t %lf\t %lf\n",temps[l], crl[l],dSigma[l]);
	 fprintf( stream2, "%lf\t %lf\t %lf\n", temps[l], crl[l],dSigma[l] );
	
		
	} 
	fclose(stream2);

	//////////////////////////Write parameters in results_correl.out///////////////////////////



	N_apparent=1/(crl[1]-1);
  
	nmoyen=1.0*Delayed_monitor[0][0]/NPTS;
	moyen=fopen( "results_correl.out","w+");
	fprintf(moyen," nombre moyen de photons: %lf\n temps de diffusion apparent:%lf\n N_apparent:%lf\n",nmoyen, t_apparent,N_apparent);
	fclose(moyen);

return 0;	
}




//----------------------



#undef IA
#undef IM
#undef AM
#undef IQ
#undef IR
#undef NTAB
#undef NDIV
#undef EPS
#undef RNMX
#undef PI

#undef EPS2

#undef lambda
#undef wxy
#undef NPTS
#undef delta_t
