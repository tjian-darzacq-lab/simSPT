#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <float.h>
#include <gsl/gsl_rng.h>

bool sphere_point_in_geometry(struct point p, struct geometryInit pars);
struct point sphere_get_random_initialization_point(struct point p,
						    struct geometryInit pars,
						    gsl_rng *prng, bool sim2d);
struct geometryInit sphere_initialize_geometry(struct geometryInit ret, struct geometryPars pars);
struct point sphere_confine_translocation(struct point pp, struct point cp,
					  struct geometryInit geometry_init,
					  gsl_rng *prng);

// Helper functions
bool RaySphere(point p1, point p2, point sc,double r,double *mu1,double *mu2); // By Paul Bourke (1992)
