#include <stdlib.h>
#include <stdio.h>
#include <gsl/gsl_rng.h>

//// Function prototypes
void initializeGeometry(struct geometryPars pars,
			struct geometryInit *geometry_init);
struct point get_random_initialization_point(struct geometryInit pars,
					     gsl_rng *prng, bool sim2d);
struct point confine_translocation(struct point pp, struct point cp,
				   struct geometryInit geometry_init,
				   gsl_rng *prng);
