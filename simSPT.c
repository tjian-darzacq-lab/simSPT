// SimSPT, a C version
// SimSPT simulates realistic single particle tracking datasets
// Parameters are detailed below
// by MW, Jun 2017, GPLv3+

// Compilation: compile with
// gcc -o simSPT simSPT.c geometries.c geometries_sphere.c distributions.c z_profiles.c geometries_cube.c -lm

// About the stopCriterion parameter
// - 0: number of particles
// - 1: number of trajectories (ie: number of detected trajectories)
// - 2: number of detections
// - 3: number of positions (not necessarily detected)

// Includes
#include "simSPT_structs.h"
#include "geometries.h"
#include "distributions.h"
#include "z_profiles.h"
#include "heterogeneous.h"
#include "simSPT.h"
#include "command_line.h"
#include "gillespie.h"

#include "fbmtd/hosking.h"
#include "fbmtd/circulant.h"

//// ==== Parameters
// Some of these parameters are accessible from the command line (cli).
// When this is the case, the corresponding command-line option is
//+specified as ("cli: "). When the parameter is specified on the command
//+line, this replaces the default indicated below.
// When parameters are changed in this file, note that you need to recompile
//+simSPT in order for these changes to take effect.

struct geometryPars geometry_params = {
  .geometry = 0, // 0: sphere, 1: cube 
  .radius = 4.0, // The radius of the Sphere, in \mu m
  .sim3d = true // Whether we should simulate the third dimension or stick it to 0.
};

struct hiloPars hilo_params = {
  .width = 4.0, // The width of the HiLo beam, in \mu m
  .bleachProb = 0.01 // The probability of the particle to "age" if not in the HiLo volume
};

struct detectionPars detection_params = {
  .profileType = 1, // 0: Gaussian, 1: Flat-Top Gaussian
  .fwhm = 0.1, // In \mu m
  .plateau = 0.6, // In \mu m
  .resolution = 300, // The number of points to sample
};

struct simulationPars simulation_params =
  {
    .dt = 0.013, // The exposure time, in seconds (cli: -dt)
    .gaps = 1, // The number of gaps allowed
    .locerror = 0.035 // The pointing accuracy, in \mu m (cli: -sigma)
  };

float beta = 4.0; // The fluorophore lifetime
float betaUnit = 0; // The unit of beta (0: frames, 1: s)

char *path = "./out.txt"; // File where the detections are saved (cli: -file)
bool save = true;
bool exportZ = false; // overriden by command line

int seed=-1; // The seed for the PRNG. Set to -1 to use a random seed (current time). (cli: -seed)

long fbm_bufsize = 512; // Size of the buffer for the fBm generation
//
// ==== Internal parameters (do not edit)
//
struct savePars *save_params_pt; // Will need to be realloc'd when we know the length of the content
struct cluster *clusters_pt;
int n_clusters = 0;
bool use_clusters_file = false;
char *clusters_file;
gsl_rng *prng;

int stopCriterion = 1; // see above
int stopCriterionValue = 10000;

struct detection pP, cP; // Previous and current detections
struct summaryStats summary_stats;

int program_state;

struct fbm_buffer fbm_buf; // fbm stuff
long fbm_seed1;
long fbm_seed2;

bool criterion(int stopCriterion, int stopCriterionValue, int n_part, int n_traj, int n_det, int n_pos) {
  if (stopCriterion==0 && (n_part >= stopCriterionValue-1)) {
    return false;
  } else if (stopCriterion==1 && (n_traj >= stopCriterionValue-1)){
    return false;
  } else if (stopCriterion==2 && (n_det >= stopCriterionValue-1)){
    return false;
  } else if (stopCriterion==3 && (n_pos >= stopCriterionValue-1)){
    return false;
  }
  return true;
}

int choose_a_population(struct diffusionPars *pops, int n_pops, gsl_rng *prng) {
  float cumsum = pops[0].p;
  double random = gsl_rng_uniform(prng);
  int i = 0;
  while (random > cumsum) {
    i++;
    cumsum += pops[i].p;
  };
  return i;
}

int choose_a_cluster(struct cluster **clusters, gsl_rng *prng) {
  float cumsum = (*clusters)[0].prob;
  double random = gsl_rng_uniform(prng);
  int i = 0;
  while (random > cumsum) {
    i++;
    //printf("%d, %f, %f\n", i, cumsum, random);
    cumsum += (*clusters)[i].prob;
  };
  //printf("Selecting cluster %d", i);
  return i;
}

bool detect_particle(struct detection p, struct zprofileInit z_profile) {
  if (z_profile.z_prob[(int)round((p.point.z-z_profile.min_z)/z_profile.step)]>p.detectFloat)
    {
      return true;
    }
  else {
    return false;
  }
}

struct savePars initialize_save_file(struct savePars pars) {
  // Open file
  if (pars.save==true) {
    pars.f = fopen(pars.path, "w");
    if (pars.f == NULL)
      {
	printf("Error opening file!\n");
	exit(1);
      }
    //printf("Saving detections to %s\n", pars.path);
    if (pars.exportZ) {
      fprintf(pars.f, "x,y,z,t,frame,trajectory\n"); // Write headers
    } else {
      fprintf(pars.f, "x,y,t,frame,trajectory\n"); // Write headers
    }
  } else {printf("Not saving the results\n");}
  return pars;
}
bool save_particle(struct detection p, struct savePars pars) {
  if (pars.save==true) {
    if (pars.exportZ) {
      fprintf(pars.f, "%f,%f,%f,%f,%ld,%ld\n", p.point.x, p.point.y, p.point.z,
	      p.t, p.f, p.traj);
    } else {
      fprintf(pars.f, "%f,%f,%f,%ld,%ld\n", p.point.x, p.point.y, p.t, p.f, p.traj);
    }
    return true;
  } else {
    return false;
  }
}
void deinitialize_save_file(struct savePars pars) {
  if (pars.save==true) {
    fclose(pars.f);
  }
}

bool particle_in_hilo_volume(struct detection cP, struct hiloPars hilo_params,
			     gsl_rng *prng) {
  if (cP.point.z < hilo_params.width/2 && cP.point.z > -hilo_params.width/2) {
    return true;

  } else if (gsl_rng_uniform(prng)<hilo_params.bleachProb) {
    return true;
  } else {
    return false;
  };
}

void move_particle(struct detection *cP, double coef,
		   struct diffusionPars *pops, int curr_pop,
		   struct fbm_buffer fbm_buf, point offset,
		   float *t, float dt) {

  if (pops[curr_pop].diffusiontype==0) { // Brownian
    (*cP).point.x += coef*gaussrand_zig(prng); // Move randomly scale by coef=sqrt(2Ddt)
    (*cP).point.y += coef*gaussrand_zig(prng);
    (*cP).point.z += coef*gaussrand_zig(prng);
  } else if (pops[curr_pop].diffusiontype==1) { // fBm
    (*cP).point.x = fbm_buf.x[(*cP).f]+offset.x;
    (*cP).point.y = fbm_buf.y[(*cP).f]+offset.y;
    (*cP).point.z = fbm_buf.z[(*cP).f]+offset.z;
  } else if (pops[curr_pop].diffusiontype==2) { // CTRW
    if (*t > dt) { // We do not move at this step
      *t -= dt;
    } else { // We make a single jump
      (*cP).point.x += coef*gaussrand_zig(prng)/sqrt(2); // Move randomly scale by coef=sqrt(2Ddt)
      (*cP).point.y += coef*gaussrand_zig(prng)/sqrt(2);
      (*cP).point.z += coef*gaussrand_zig(prng)/sqrt(2); // No idea why we need to rescale by sqrt(2) ?!
      
      //*t += power(dt/100., dt*100, pops[curr_pop].ctrw_alpha, prng); // Compute the time of next jump
      *t += power(dt/100., dt, pops[curr_pop].ctrw_alpha, prng); // Compute the time of next jump
      move_particle(cP, coef, pops, curr_pop, fbm_buf, offset, t, dt); // recursive call
    }
  }
}

struct summaryStats make_trajectories_clusters(int stopCriterion,
					       int stopCriterionValue,
		       struct diffusionPars *pops, int n_pops, double popsK[],
		       struct geometryInit geometry_init,
		       struct simulationPars simulation_params,
		       struct zprofileInit z_profile,
		       struct savePars save_params, struct hiloPars hilo_params,
		       struct summaryStats summary_stats, bool use_gillespie,
		       struct cluster **clusters, int n_clusters,
		       bool use_clusters_file,
		       gsl_rng *prng, long *seed1, long *seed2) {
  int i;
  long n_part = 1; // Number of simulated particles
  long n_traj = 0; // Index of current trajectory
  long nn_traj = 0;// Number of (detected) trajectories
  long n_det = 0;  // Number of detections
  long n_pos = 1;  // Number of positions (not necessarily detected)
  int curr_pop;    // The current population from which we generate a trajectory
  int curr_cluster = -1;// The current cluster from which we generate a trajectory
  int lifetime;    // The max lifetime of the particle
  float t, tt;     // GILLESPIE: the time between the intervals
  int old_pop;     // GILLESPIE: memory of the previous population
  struct geometryInit geometry_clusters;
  geometry_clusters.geometry = 0;
  int gaps = simulation_params.gaps;
  float dt = simulation_params.dt;
  float sigma = simulation_params.locerror;
  float coef;
  int curr_gap;
  point offset;
  long prev_traj=0;
  struct detection tmpd;
  
  // Now the master loop
  while (criterion(stopCriterion, stopCriterionValue, n_part, nn_traj, n_det, n_pos)==true)
  {
    // Choose a population to draw from
    // 1. Compute the ratio of volumes, weighted by the enrichment ratio
    // 2. Compute a probability vector -> Should be in the struct, with volume and
    // +prob entries (float). The latest element of the struct has a type -1
    // +meaning outside clusters
    // 3. Choose a cluster
    // 4. Choose a population (wrt cluster)
    if (use_clusters_file) {
      curr_cluster = choose_a_cluster(clusters, prng);
      pops = (*clusters)[curr_cluster].pops;
      point p = (*clusters)[curr_cluster].point;
      double r = (*clusters)[curr_cluster].r;
      geometry_clusters.max_x = p.x+r;
      geometry_clusters.min_x = p.x-r;
      geometry_clusters.max_y = p.y+r;
      geometry_clusters.min_y = p.y-r;
      geometry_clusters.max_z = p.z+r;
      geometry_clusters.min_z = p.z-r;
    }    
    curr_pop = choose_a_population(pops, n_pops, prng);
    coef = sqrt(2*pops[curr_pop].D*dt);

    // Draw a particle (with a >0 lifetime)
    lifetime = 0;
    while (lifetime < 2) {
      // Generate lifetime
      lifetime = exponential(pops[curr_pop].beta, prng);
      if (pops[curr_pop].betaUnit==1) {
	lifetime = (int) (lifetime/dt);
      }

      // Generate particle
      pP.t = 0;
      pP.f = 0;
      pP.traj = n_traj;
      pP.point = get_random_initialization_point(geometry_init, prng, !geometry_params.sim3d);
      if (!geometry_params.sim3d) { // simulate in 2D
	pP.point.z = 0;
      }
      offset.x = 0;
      offset.y = 0;
      offset.z = 0;

      long fbm_n = ceil(log2(fbm_bufsize));
      int cum = 1;
      double L = pow(2, fbm_n);
      double H = pops[curr_pop].fbm_H;
      if ((pops[curr_pop].diffusiontype==1) & (lifetime>1)) { // Generate a fBm trajectory
	if (lifetime < fbm_buf.size) {
	  hosking(&fbm_n, &H, &L, &cum, seed1, seed2, fbm_buf.x);
	  hosking(&fbm_n, &H, &L, &cum, seed1, seed2, fbm_buf.y);
	  hosking(&fbm_n, &H, &L, &cum, seed1, seed2, fbm_buf.z);
	  for (i=0;i<(fbm_bufsize-1);i++) {
	    fbm_buf.x[i] = fbm_buf.x[i]*coef + pP.point.x;
	    fbm_buf.y[i] = fbm_buf.y[i]*coef + pP.point.y;
	    fbm_buf.z[i] = fbm_buf.z[i]*coef + pP.point.z;
	  }
	} else {
	  printf("Buffer too small. Not implemented error.\n");
	  exit(1);
	  /* fbm_buf.bigx = realloc(fbm_buf.bigx, */
	  /* 			 pow(2, ceil(log2(lifetime)))*sizeof(double)); */
	  /* fbm_buf.bigy = realloc(fbm_buf.bigy, */
	  /* 			 pow(2, ceil(log2(lifetime)))*sizeof(double)); */
	  /* fbm_buf.bigz = realloc(fbm_buf.bigz, */
	  /* 			 pow(2, ceil(log2(lifetime)))*sizeof(double)); */
	  /* hosking(&fbm_n, &H, &L, &cum, &seed1, &seed2, fbm_buf.bigx); */
	  /* hosking(&fbm_n, &H, &L, &cum, &seed1, &seed2, fbm_buf.bigy); */
	  /* hosking(&fbm_n, &H, &L, &cum, &seed1, &seed2, fbm_buf.bigz); */
	  /* printf("WARNING: Wrong seeds"); */
	  
	}
      }
      
      if (use_clusters_file) { // TODO MW: Section to be moved to heterogeneous.c
	if (curr_cluster!=n_clusters) {
	  while (!point_in_cluster(pP.point, clusters, curr_cluster, n_clusters)) {
	    pP.point.x = (geometry_clusters.max_x-geometry_clusters.min_x)
	      * (float)gsl_rng_uniform(prng)
	      + geometry_clusters.min_x;
	    pP.point.y = (geometry_clusters.max_y-geometry_clusters.min_y)
	      * (float)gsl_rng_uniform(prng)
	      + geometry_clusters.min_y;
	    if (geometry_params.sim3d) {
	      pP.point.z = (geometry_clusters.max_z-geometry_clusters.min_z)
		* (float)gsl_rng_uniform(prng)
		+ geometry_clusters.min_z;
	    } else {
	      pP.point.z = 0; 
	    }
	  }
	} else {
	  while (!point_in_cluster(pP.point, clusters, curr_cluster, n_clusters)) {
	    pP.point = get_random_initialization_point(geometry_init, prng, !geometry_params.sim3d);
	    if (!geometry_params.sim3d) {
	      pP.point.z = 0; // TODO MW: WARNING, this might loop forever!!!
	    }
	  }
	}
      }
      pP.detectFloat = gsl_rng_uniform(prng);
      pP.lifetimeMax = lifetime;
      pP.lifetimeAge = 1;
      n_pos++;

      // If this is an aborted particle, see if we should save it.
      if ((pP.lifetimeMax==pP.lifetimeAge) && detect_particle(pP, z_profile)==true) {
	tmpd = pP; // Add localization error (use an independent point)
	tmpd.point.x += gaussrand_zig(prng)*sigma;
	tmpd.point.y += gaussrand_zig(prng)*sigma;
	if (geometry_params.sim3d) {
	  tmpd.point.z += gaussrand_zig(prng)*sigma;
	} else {
	  tmpd.point.z = 0;
	}
      	save_particle(tmpd, save_params);
	n_det++;

	nn_traj++;
	n_traj++;
	prev_traj = tmpd.traj;

      };

      // Increment the particle number. Note that this includes particle we
      //+we never see (lifetimeMax=0).
      n_part ++;  
    };

    // Save pP (previous particle) before simulating the whole trajectory
    if (detect_particle(pP, z_profile)==true) {
      //printf("%f %f %f\n", pP.point.x, pP.point.y, pP.point.z);
      tmpd = pP; // Add localization error (use an independent point)
      if (pops[curr_pop].diffusiontype == 1) {
	tmpd.point.x = fbm_buf.x[0];
	tmpd.point.y = fbm_buf.y[0];
	if (geometry_params.sim3d) {
	  tmpd.point.z = fbm_buf.z[0];
	} else {
	  tmpd.point.z = 0;
	}
      }

      tmpd.point.x += gaussrand_zig(prng)*sigma; // adding localization error
      tmpd.point.y += gaussrand_zig(prng)*sigma;
      tmpd.point.z += gaussrand_zig(prng)*sigma;
      //printf("%f %f %f\n", pP.point.x, pP.point.y, pP.point.z);
      save_particle(tmpd, save_params);
      n_det++;
      if (tmpd.traj!=prev_traj) {
	nn_traj++;
	prev_traj = tmpd.traj;
      }
    };
    t = 0;
    tt = 0;
    old_pop = curr_pop;
    curr_gap = 0;
    cP.lifetimeAge = 1;
    cP.lifetimeMax = pP.lifetimeMax;
    cP.traj = pP.traj;
    cP.point = pP.point;
    while (cP.lifetimeAge < cP.lifetimeMax) {
      // Instanciate the new guy (cP)
      cP.f = pP.f + 1;
      cP.t = cP.f*dt;
      cP.detectFloat = gsl_rng_uniform(prng);

      // Move
      if (!use_gillespie) {
	if ((cP.f>=fbm_bufsize) & (pops[curr_pop].diffusiontype == 1)) {
	  printf("ERROR, too big");
	  exit(1);
	}
	move_particle(&cP, coef, pops, curr_pop, fbm_buf, offset, &t, dt);
      } else { // Gillespie only handles Brownian diffusion so far.
	gillespie_move(&t, &tt, &cP.point, &curr_pop, &old_pop,
		       dt, pops, popsK, n_pops, prng);
      }
      if (!geometry_params.sim3d) { // simulate 2D
	tmpd.point.z = 0;
	cP.point.z = 0;
      }
      n_pos++;

      // Confine the particle
      tmpd = cP;
      cP.point = confine_translocation(pP.point, cP.point, geometry_init, prng);
      if (pops[curr_pop].diffusiontype == 1) {
	offset.x += cP.point.x-tmpd.point.x;
	offset.y += cP.point.y-tmpd.point.y;
	offset.z += cP.point.z-tmpd.point.z;
	//printf("%f, %f, %f, %f\n", cP.point.x, tmpd.point.x, offset.x,       fbm_buf.x[cP.f]);
      }
      
      // Update the Age of the particle (if illuminated)
      if (particle_in_hilo_volume(cP, hilo_params, prng)==true) {
	  cP.lifetimeAge = pP.lifetimeAge+1; // Only if in the illumination (HiLo) volume
      };

      // Save particle if we can see it
      if (detect_particle(cP, z_profile)==true) {
	tmpd = cP; // Add localization error (use an independent point)
	tmpd.point.x += gaussrand_zig(prng)*sigma;
	tmpd.point.y += gaussrand_zig(prng)*sigma;
	tmpd.point.z += gaussrand_zig(prng)*sigma;
	if (tmpd.traj!=prev_traj) {
	  nn_traj++;
	  prev_traj = tmpd.traj;
	}
	save_particle(tmpd, save_params);
	n_det++;
      } else {
	curr_gap++;
	if (curr_gap>gaps) {
	  curr_gap = 0; 
	  cP.traj++;
	  n_traj++;
	}
      }

      // Change the state of the particle if it moved to another cluster
      // special case for the last cluster
      if (use_clusters_file) {
	if (!point_in_cluster(cP.point, clusters, curr_cluster, n_clusters)) {
	  curr_cluster = point_in_which_cluster(cP.point, clusters, n_clusters);
	  pops = (*clusters)[curr_cluster].pops;
	  coef = sqrt(2*pops[curr_pop].D*dt);
	}
      }
      
      // Shallow copy from cP to pP
      pP = cP;
      pP.point = cP.point;
    };
    n_traj++;
  };
  summary_stats.n_traj = nn_traj;
  summary_stats.n_part = n_part;
  summary_stats.n_det = n_det;
  summary_stats.n_pos = n_pos;
  return summary_stats;
}


int main(int argc, char *argv[]) {
  // Instanciations
  int i; int j;
  int n_pops;
  struct diffusionPars *pops;
  double *popsK;
  struct geometryInit geometry_init;
  struct zprofileInit zprofile_init;
  bool use_gillespie=false;
    
 // ==== Parameters
  save_params_pt = realloc(save_params_pt, sizeof(struct savePars));
  save_params_pt->path = path;
  save_params_pt->save = save;
  save_params_pt->exportZ = exportZ;

  
  fbm_buf.size = fbm_bufsize; // Initializing fractional Brownian motion
  fbm_buf.bigsize = 0;
  fbm_buf.x = malloc(fbm_bufsize*sizeof(double));
  fbm_buf.y = malloc(fbm_bufsize*sizeof(double));
  fbm_buf.z = malloc(fbm_bufsize*sizeof(double));
  for (i=0;i<fbm_bufsize;i++) {
    fbm_buf.x[i]=0;
    fbm_buf.y[i]=0;
    fbm_buf.z[i]=0;
  }
  // ==== Reading from the command line arguments and start the appropriate
  //      Segment of the program.
  program_state = read_command_line(argc, argv);
  n_pops = read_command_line_nstates(argc, argv, &use_gillespie);

  if (program_state == 0) { // Simulate brownian trajectories as usual
    pops = malloc(n_pops*sizeof(struct diffusionPars));
    popsK = malloc(n_pops*n_pops*sizeof(double));

    // TODO MW: move that hell to a function initialize_transitions(pops, popsK)
    for (i=0;i<n_pops;i++) { // Initialize transition rates
      for (j=0;j<n_pops;j++) {
	popsK[i*n_pops+j]=0.0;
      }
    }
    for (i=0;i<n_pops;i++) { // Initialize (p, D)
      pops[i].p = 0;
      pops[i].D = 0;
      pops[i].beta = beta;
      pops[i].betaUnit = betaUnit;
      pops[i].diffusiontype = 0;
    }
    
    if (n_pops == 1) {
      pops[0].p = 1.0;
      pops[0].D = 2.0;
    } else if (n_pops == 2) {
      pops[0].p = 0.3;
      pops[0].D = 0.001;
      pops[1].p = 0.7;
      pops[1].D = 2.0;
    }

    read_command_line_simulation(argc, argv, popsK, pops, 
    				 &simulation_params, &geometry_params,
				 save_params_pt, &beta,
    				 &stopCriterion, &stopCriterionValue,
    				 &seed, n_pops, use_gillespie,
    				 &clusters_file, &use_clusters_file);
    if (use_clusters_file) {
      if (n_pops != 2) {
	printf("When using a clusters file, exactly two states should be specified. ABORTING");
	exit(1);
      }
      n_clusters = read_clusters_input_file(clusters_file, use_clusters_file,
					    &clusters_pt, geometry_params, pops);
    }
    
  } else if (program_state == 1) { // Generate an heterogeneous nucleus
    read_command_line_make_heterogeneous(argc, argv);
  } else if (program_state == 2) { // Print the z profile
    // do nothing
  } else {
    printf("Unrecognized command line arguments, exiting\n");
    exit(1);
  }

  // ==== Initializations
  initializeGeometry(geometry_params, &geometry_init); // Initialize geometry
  initialize_z_profile(detection_params, geometry_init, &zprofile_init);

  struct savePars save_params = initialize_save_file(*save_params_pt);

  prng = gsl_rng_alloc(gsl_rng_mt19937); // Instantiate a Mersenne-Twister PRNG
  if (seed==-1) {
    srand48(time(NULL));  // Initialize PRNG (old)
    gsl_rng_set(prng,time(NULL));
    fbm_seed1 = time(NULL);
    fbm_seed2 = time(NULL);
  } else {
    srand48(seed);
    gsl_rng_set(prng,seed);
    fbm_seed1 = seed;
    fbm_seed2 = 2*seed;
    printf("PRNG not initialized, using seed %d\n", seed);
  }
  
  // ==== Display welcome panels
  if (program_state == 1) { // Cluster generation mode, run & exit
    print_command_line_summary_make_heterogeneous(geometry_init);
    make_heterogeneous_nucleus(geometry_init, prng);

    // Free & exit
    deinitialize_save_file(save_params);
    free(zprofile_init.z_prob);
    free(save_params_pt);
    gsl_rng_free(prng);
    exit(EXIT_SUCCESS);
  } else if (program_state == 2) {
    print_zprofile(detection_params, geometry_init, zprofile_init);
    exit(EXIT_SUCCESS);
  } else if (program_state == 0) { // Brownian simulation mode
    
    // ==== Consistency checks
    // Compute equilibrium statistics from the rates & potentially complain
    int res;
    if (use_gillespie) {
      res = get_equilibrium(popsK, n_pops, pops);
      if (res != 0) { // We should dereference/free before exiting.
	exit(1);
      }
    }
  
    float sumpop = 0;
    // Test for the sum of populations to 1
    for (i=0; i<n_pops; i++) {sumpop += pops[i].p;}
    if (fabs(sumpop-1) > FLT_EPSILON) {
      printf("The proportions of the %d populations do not sum to 1, they sum to %f.\n", n_pops, sumpop);
      printf("Exiting\n");
      exit(1);
    }

    // ==== Printing status
    printf("==== Running with parameters: ====\n");
    printf("Simulation volume of radius: %f µm\n", geometry_params.radius);
    printf("Number of subpopulations: %d\n", n_pops);
    for (i=0;i<n_pops;i++) {
      if (pops[i].diffusiontype == 0) {
	printf("- Subpopulation %d: D=%f µm²/s, p=%f\n", i+1, pops[i].D, pops[i].p);
      } else if (pops[i].diffusiontype == 1) {
	printf("- Subpopulation %d (fBm): D=%f µm²/s, p=%f, H=%f\n", i+1,
	       pops[i].D, pops[i].p, pops[i].fbm_H);
      } else if (pops[i].diffusiontype == 2) {
	printf("- Subpopulation %d (CTRW): D=%f µm²/s, p=%f, a=%f\n", i+1,
	       pops[i].D, pops[i].p, pops[i].ctrw_alpha);
      }
    }
    
    printf("dt: %f s\n", simulation_params.dt);
    printf("sigma: %f µm\n", simulation_params.locerror);
    if (betaUnit==0) {
      printf("beta (lifetime): %f frames\n", beta);
    } else {
      printf("beta (lifetime): %f s\n", beta);
    }
    printf("n_traj: %d\n", stopCriterionValue);
    printf("file: %s\n", save_params_pt->path);
    printf("PRNG seed: %d\n", seed);
    if (use_clusters_file) {
      printf("Reading a cluster file from: %s\n", clusters_file);
      printf("This file contains %d clusters\n", n_clusters);

      for (i=0;i<(n_clusters);i++) {
	printf("-- cluster %d:\tradius %f µm, enrich %f, Fbound %f, p %f\n",
	       clusters_pt[i].cluster,
	       clusters_pt[i].r,
	       clusters_pt[i].enrich,
	       clusters_pt[i].pops[0].p,
	       clusters_pt[i].prob);
      }
      
    } else {
      printf("Not reading a clusters file.\n");
    }
    printf("==== ====\n");
  }
  
  // ==== Generate the trajectories
  printf("-- Let's go, simulate trajectories!\n");
  summary_stats = make_trajectories_clusters(stopCriterion, stopCriterionValue,
					     pops, n_pops, popsK, geometry_init,
					     simulation_params, zprofile_init,
					     save_params, hilo_params,
					     summary_stats, use_gillespie,
					     &clusters_pt, n_clusters,
					     use_clusters_file, prng,
					     &fbm_seed1, &fbm_seed2);
    
  printf("-- Done!\n");
  
  // ==== Exit cleanly & print some statistics
  deinitialize_save_file(save_params);
  free(save_params_pt);
  free(clusters_pt);
  free(pops);
  free(popsK);
  free(zprofile_init.z_prob);
  free(fbm_buf.x);
  free(fbm_buf.y);
  free(fbm_buf.z);
  if (fbm_buf.bigsize != 0) {
    free(fbm_buf.bigx);
    free(fbm_buf.bigy);
    free(fbm_buf.bigz);

  }
  gsl_rng_free(prng);
  
  printf("==== Summary statistics ====\n");
  printf("Number of simulated trajectories: %ld\n", summary_stats.n_part);
  printf("Number of observed trajectories: %ld\n", summary_stats.n_traj);
  printf("Number of observed detections: %ld\n", summary_stats.n_det);
  printf("Number of simulated detections: %ld\n", summary_stats.n_pos);
  return EXIT_SUCCESS;
}
