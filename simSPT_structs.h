#include <stdbool.h>
#include <stdio.h>

//// Structure definition
//struct point {float x; float y; float z;}; // A simple 3D point
//typedef struct point point;
struct pointDB {double x; double y; double z;}; // A simple 3D point
typedef struct pointDB pointDB;
struct point {double x; double y; double z;}; // Redundant definition, transitional
typedef struct point point;

struct hiloPars {
  float width;
  float bleachProb;
};

struct detectionPars {
  float profileType;
  float fwhm;
  float plateau;
  int resolution;
};

struct diffusionPars {
  float p; // The fraction of this population, in [0,1]
  float D; // The diffusion coefficient, in \mu m^2/s
  float beta; // The fluorophore lifetime
  float betaUnit; // The unit of beta (0: frames, 1: s)
  int   diffusiontype; // The type of diffusion (0: Brownian, 1: fBm)
  float fbm_H; // The Hurst coefficient (fBm only)
  float ctrw_alpha; // The alpha coefficient (CTRW only)
};  

struct simulationPars
{
  float dt;
  int gaps;
  float locerror;  
};

struct detection
{
  struct point point; // x, y, z position
  float t;   // time
  long f;    // frame number
  long traj; // trajectory id
  float detectFloat; // a random number between 0 and 1, used to know if the particle was detected
  int lifetimeMax; // Max lifetime of the particle
  int lifetimeAge; // The age of the particle
} cP, pP; // Current and previous particle

//// Structs
struct geometryPars {
  int geometry;
  float radius;
  float edge;
  bool sim3d;
};

struct geometryInit {
  int geometry;
  float max_x;
  float min_x;
  float max_y;
  float min_y;
  float max_z;
  float min_z;
  double sphere_radius; // sphere specific parameters
};

struct zprofileInit {
  struct detectionPars pars;
  float *z_prob;
  float min_z;
  float step;
};

// Contains saving parameters
struct savePars {
  char *path;
  FILE *f;
  bool save;
  bool exportZ; // if the third dimension should be exported
};


// Contains statistics
struct summaryStats {
  long n_part;
  long n_traj;
  long n_det;
  long n_pos;
};

// Cluster-related
struct cluster {
  struct point point; // x, y, z position
  double r;
  float boundC; // Bound fraction
  float enrichC; // Enrichment ratio
  int nattempts;
  // Add a population object in this guy...
  int nucleus;
  int cluster;
  float enrich;
  int n_pops;
  struct diffusionPars pops[2];
  double area;
  double prob;
};

struct fbm_buffer {
  long size;
  long bigsize;
  double *x;
  double *y;
  double *z;
  double *bigx;
  double *bigy;
  double *bigz;
  
};
