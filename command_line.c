#include "simSPT_structs.h"
#include "command_line.h"
#include "version.h"
#include "gillespie.h"

// Collect everything related to the parsing of the command line arguments

/* Simple function to determine in which mode should the program behave */
/*  0: simulate brownian motion */
/*  1: generate a model of an heterogeneous nucleus (makeclusters) */
/*  2: display the z profile */
/* -1: something went wrong and we don't know how to proceed. */

int read_command_line(int argc, char *argv[]) {
  if (argc<2) {
    return 0;
  }
  if (strncmp(argv[1], "makeclusters", 13)==0) {
    return 1;
  } else if (strncmp(argv[1], "zprofile", 13)==0) {
  return 2;
  } else if (strncmp(argv[1], "-", 1)==0) {
    return 0;
  } else {
    return -1;
  }
}

/* Detect the number of states declared in the command line */
/* If nothing is specified, fallback to the default 2 states model */
int read_command_line_nstates(int argc, char *argv[], bool *use_gillespie) {
  // Count number of states
  int nsta = 1;
  int cnsta = nsta;
  int i = 1;
  int j = 0;
  for (i=1;i<argc;i++) { // Test if the parameter starts with "-Dx"
    if (strncmp(argv[i], "-D", 2)==0) {
      cnsta = atoi(&(argv[i][2])); // For some undocumented reasons, this works :|
      if (cnsta>nsta) {
	nsta = cnsta;
      }
    }
  }

  // We have default values for nsta==1 and nsta==2
  if (nsta<2) {
    return nsta;
  }

  // Validate that we have the info.
  char bufD[10]; // Should be enough for a bunch of states
  char bufP[10];
  char bufK[10];
  bool foundD = false;
  bool foundP = false;
  bool foundK = false;
  bool anyP = false;
  bool anyK = false;
  
  for (i=1;i<=nsta;i++) {
    foundD = false;
    foundP = false;
    foundK = false;
    for (j=1;j<argc;j++) {
      sprintf(bufD, "-D%d=", i);
      sprintf(bufP, "-p%d=", i);
      sprintf(bufK, "-k%d_", i);
      if (strncmp(bufD, argv[j], strlen(bufD))==0) {
	foundD = true;
      } else if (strncmp(bufP, argv[j], strlen(bufP))==0) {
	foundP = true;
	anyP = true;
      } else if (strncmp(bufK, argv[j], strlen(bufK))==0) {
	foundK = true;
	anyK = true;
      }
    }
    if (!foundD) {
      printf("ERROR: %d subpopulations seem to be declared, but could not find the -D%d argument.\n", nsta, i);
      exit(1);
    }
    if ((!foundP) & (!foundK)) {
      printf("ERROR: %d subpopulations seem to be declared, but could not find the -p%d argument.\n", nsta, i);
      exit(1);
    }
  }

  if (anyP & anyK) {
    printf("ERROR: Both a fraction (-p) and a kinetic rate (-k) are declared.\n");
    exit(1);
  }

  if ((anyP) | (nsta==1)) { // Set whether we should use the Gillespie algorithm.
    *use_gillespie = false;
  } else if (anyK) {
    *use_gillespie = true;
  }
  return nsta;
}

// This function is called if no specific keyword is passed to the
//+executable on the command line. It triggers the simulation of
//+brownian motion.
void read_command_line_simulation(int argc, char *argv[], double popsK[],
				  struct diffusionPars *pops, 
				  struct simulationPars *simulation_params,
				  struct geometryPars *geometry_params,
				  struct savePars *save_params_pt, float *beta,
				  int *stopCriterion, int *stopCriterionValue,
				  int *seed, int n_pops, int use_gillespie,
				  char **clusters_file, bool *use_clusters_file) {
  int i=0;
  int j,k;
  int diffusiontype = -1;
  float H=-1;
  float alpha=-1;
  char bufD[10]; // Should be enough for a bunch of states
  char bufP[10];
  char bufK[10];
  char *token;
  char *search = "=";

  // Check that we use all the parameters
  bool used = true; // the first argument is the name of the program
  
  for (i=0;i<argc;i++) {
    for (j=1;j<=n_pops;j++) { // Populate all the D & P
      sprintf(bufD, "-D%d=", j);
      sprintf(bufP, "-p%d=", j);      
      if (strncmp(bufD, argv[i], strlen(bufD))==0) {
	token = strtok(argv[i], search);
	token = strtok(NULL, search);
	pops[j-1].D = atof(token);
	used=true;
      } else if ((n_pops>1) & (!use_gillespie) & (strncmp(bufP, argv[i], strlen(bufP))==0)) {
	token = strtok(argv[i], search);
	token = strtok(NULL, search);
	pops[j-1].p = atof(token);
	used=true;
      }
      pops[j-1].diffusiontype = 0; // By default we do Brownian simulation
    }
    
    // Populate all the K available
    if (use_gillespie) {
      for (j=1;j<=n_pops;j++) {
	for (k=1;k<=n_pops;k++) {
	  sprintf(bufK, "-k%d_%d=", j, k);
	  if (strncmp(bufK, argv[i], strlen(bufK))==0) {
	    token = strtok(argv[i], search);
	    token = strtok(NULL, search);
	    used=true;
	    //printf("K(%d,%d)=%f (%d)\n", j, k, atof(token), idx(j,k, n_pops));
	    popsK[idx(j,k, n_pops)] = atof(token);
	  }
	}
      }
    }

    if (strncmp(argv[i], "-sigma=", 7)==0) {
      simulation_params->locerror = atof(&(argv[i][7]));
      used=true;
    } else if (strncmp(argv[i], "-dt=", 4)==0) {
      simulation_params->dt = atof(&(argv[i][4]));
      used=true;
    } else if (strncmp(argv[i], "-n_traj=", 8)==0) {
      *stopCriterionValue = (int)atof(&(argv[i][8]));
      *stopCriterion = 1;
      used=true;
    } else if (strncmp(argv[i], "-clusters=", 10)==0) {
      *use_clusters_file = true;
      *clusters_file = &(argv[i][10]);
      used=true;
    } else if (strncmp(argv[i], "-file=", 6)==0) {
      save_params_pt->path = &(argv[i][6]);
      used=true;
    } else if (strncmp(argv[i], "-seed=", 6)==0) {
      *seed = (int)atof(&(argv[i][6]));
      used=true;
    } else if (strncmp(argv[i], "-motion=", 8)==0) {
      used=true;
      if (strncmp(argv[i], "-motion=brownian", 16)==0) {
	printf("Type of motion: Brownian\n");
	diffusiontype = 0;
      } else if (strncmp(argv[i], "-motion=fbm", 11)==0) {
	printf("Type of motion: fractional Brownian motion (fBm)\n");
	diffusiontype = 1;
      } else if (strncmp(argv[i], "-motion=ctrw", 12)==0) {
	printf("Type of motion: Continuous Time Random Walk (CTRW)\n");
	diffusiontype = 2;
      } else {
	printf("Unrecognized type of motion, should be 'brownian', 'ctrw' or 'fbm'\n");
	printf("ABORTING\n");
	exit(1);
      }  
    } else if (strncmp(argv[i], "-H=", 3)==0) {
      H = (float)atof(&(argv[i][3]));
      if ((H<=0)|(H>=1)) {
	printf("H, the Hurst parameter, should be between in (0,1). ABORTING.\n");
	exit(1);
      }
      used=true;
    } else if (strncmp(argv[i], "-alpha=", 7)==0) {
      alpha = (float)atof(&(argv[i][7]));
      if ((alpha<=0)|(alpha>=1)) {
	printf("alpha, the anomalous diffusion coefficient, should be between (0,1). ABORTING.\n");
	exit(1);
      }
      used=true;
    } else if ((strcmp(argv[i], "-v")==0)|(strcmp(argv[i], "-version")==0)) {
      printf(version_txt, version); // Print version and exit
      exit(EXIT_SUCCESS);
    } else if (strncmp(argv[i], "-radius=", 8)==0) {
      geometry_params->radius = (float)atof(&(argv[i][8]));
      //printf("Detected radius! %f\n", geometry_params->radius);
      used=true;
    } else if (strcmp(argv[i], "-2d")==0) { // Simulate in 2D, not 3D
      geometry_params->sim3d = false;
      used = true;
    } else if (strncmp(argv[i], "-beta=", 6)==0) {
      *beta = (float)atof(&(argv[i][6]));
      for (j=1;j<=n_pops;j++) {
	pops[j-1].beta = *beta;
      }
      used = true;
    } else if (strcmp(argv[i], "-exportZ")==0) { // Export x,y,z
      save_params_pt->exportZ = true;
      used = true;
    }

    // Check that we have parsed this parameter
    if (used==false) {
      printf("WARNING! The following parameter has not been recognized %s\n", argv[i]);
    }
    used = false;
  } 
  printf("%d\n",diffusiontype);
  if ((diffusiontype==1) & (!(H>0) & (H<1))) { // CTRW
    printf("The Hurst parameter H should be between 0 and 1\n");
    printf("ABORTING.");
    exit(1);
  } else if ((diffusiontype==2) & (!(alpha>0) & (alpha<1))) {
    printf("The anomalous diffusion parameter alpha should be between 0 and 1\n");
    printf("ABORTING.");
    exit(1);
  }
  
  for (i=0;i<n_pops;i++) {
    pops[i].ctrw_alpha = alpha;
    pops[i].fbm_H = H;
    if (diffusiontype != -1) {
      pops[i].diffusiontype = diffusiontype;
    }
  }
}
