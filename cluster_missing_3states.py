#!/usr/bin/python
# -*- coding: utf-8 -*-
# by MW, GPLv3+, Aug. 2017
# Generate a series of simulations to assess the quality of a 3-states model.
import numpy as np
import os

sigma = 0.025

t = np.array([1,4,7,10,13,20])/1000.
stD1 = 0.5
strD1 = [0.5, 3]

stD2 = 1
strD2 = [4, 12]


F = [(0, 0),
     (0, 25),
     (0, 75),
     (0, 95),
     (25, 0),
     (25, 25),
     (25, 50),
     (25, 75),
     (50, 0),
     (50, 25),
     (50, 50),
     (75, 0),
     (75, 25),
 ]
F1 = np.array([i[0] for i in F])
F2 = np.array([i[1] for i in F])

bn = "./simulations/sim{}_{}"

## Compute the grid
nstD1 = int((strD1[1]-strD1[0])/stD1)
nstD10 = int(strD1[0]/stD1)

nstD2 = int((strD2[1]-strD2[0])/stD2)
nstD20 = int(strD2[0]/stD2)

p, d1, d2, tt = np.mgrid[:len(F),
                         nstD10:(nstD10+nstD1),
                         nstD20:(nstD20+nstD2),
                         :len(t)]
pp1 = F1[p.flatten()]*.01
pp2 = F2[p.flatten()]*.01
d1 = d1.flatten()*stD1
d2 = d2.flatten()*stD2
tt = t[tt.flatten()]

bn="./simulations/180812_D2{}_D3{}_p1{}_p2{}_dt{}s.csv"
pattern = "-D1=0.001 -D2={} -D3={} -p1={} -p2={} -p3={} -sigma={} -dt={} -n_traj=100000 -file={} -seed={}"

for i in range(len(d1)):
    p1 = pp1[i]
    p2 = pp2[i]
    p3 = 1-p1-p2
    D2 = d1[i]
    D3 = d2[i]
    dt = tt[i]
    bnf = bn.format(D2, D3, p1, p2, dt)
    if not os.path.isfile(bnf):
        print pattern.format(D2, D3, p1, p2, p3, sigma, dt, bnf, i)
