#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <math.h>
#include <gsl/gsl_rng.h>
#include "geometries.h"

// Function prototypes
void read_command_line_make_heterogeneous(int argc, char *argv[]);
void print_command_line_summary_make_heterogeneous();
int make_heterogeneous_nucleus(struct geometryInit geometry_init, gsl_rng *prng);
int read_clusters_input_file(char *fileName, bool use_clusters_file,
			     struct cluster **clusters_pt,
			     struct geometryPars geometry_params,
			     struct diffusionPars * pops);
bool point_in_cluster(point p, struct cluster **clusters_pt, int curr_cluster,
		      int n_clusters);
int point_in_which_cluster(point p, struct cluster **clusters_pt,
			   int n_clusters);
