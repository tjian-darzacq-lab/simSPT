// File describing a cube geometry
// This file is part of simSPT
// GPLv3+, by MW, June 2017

#include "simSPT_structs.h"
#include "geometries_cube.h"

bool cube_point_in_geometry(struct point p, struct geometryInit pars) {
  if ((p.x < pars.min_x)||(p.x>pars.max_x)||
      (p.y < pars.min_y)||(p.y>pars.max_y)||
      (p.z < pars.min_z)||(p.z>pars.max_z)) {
    return false;
  } else {
    return true;
  }
}

struct point cube_get_random_initialization_point(struct point p,
						  struct geometryInit pars,
						  gsl_rng *prng) {
  // Generate a value
  p.x = (pars.max_x-pars.min_x)*(float)gsl_rng_uniform(prng) + pars.min_x;
  p.y = (pars.max_y-pars.min_y)*(float)gsl_rng_uniform(prng) + pars.min_y;
  p.z = (pars.max_z-pars.min_z)*(float)gsl_rng_uniform(prng) + pars.min_z;
  return p;
}

struct geometryInit cube_initialize_geometry(struct geometryInit ret, struct geometryPars pars) {
  float r = pars.edge/2.;
  ret.max_x=r;
  ret.min_x=-r;
  ret.max_y=r;
  ret.min_y=-r;
  ret.max_z=r;
  ret.min_z=-r;
  return ret;
}

struct point cube_confine_translocation(struct point pp, struct point cp,
				   struct geometryInit gi) {
  int i = 0;
  while (cube_point_in_geometry(cp, gi)==false) {
    if (cp.x<gi.min_x) { cp.x = 2*gi.min_x-cp.x; }
    else if (cp.x>gi.max_x) { cp.x = 2*gi.max_x-cp.x; }
    if (cp.y<gi.min_y) { cp.y = 2*gi.min_y-cp.y; }
    else if (cp.y>gi.max_y) { cp.y = 2*gi.max_y-cp.y; }
    if (cp.z<gi.min_z) { cp.z = 2*gi.min_z-cp.z; }
    else if (cp.z>gi.max_z) { cp.z = 2*gi.max_z-cp.z; }
    if (i>10){printf("More than 10 reflections, something is wrong");}
    i++;
  };
  return cp;
};
