simSPT simulates realistic single particle tracking datasets
------------------------------------------------------------

## Overview
simSPT is a small tool to simulate single particle trajectories of freely diffusing molecules in a confined geometry (so far, a cube or a sphere) observed in fluorescence microscopy. simSPT tries to be realistic about that, that is it takes into account (uneven) photobleaching (simulating a HiLo beam and fluorophore lifetime), it also takes into account a user-defined detection probability along the z axis. Finally, simSPT can simulate state transitions (using the Gillespie algorithm). In all cases, simulations are performed starting from a steady state.

Also, simSPT is written in C, and is *fast* (that is, >>20 times faster than the corresponding Python/Matlab implementation).

**Important note:** the way simSPT generates random numbers changed between version 1.5 and 1.5.1 (bugfix) and between 1.5.1 and 1.6 (new algorithms). As such, the datasets produced by an earlier version of simSPT and where the seed `-seed` argument was specified cannot be reproduced on later versions. However, datasets with the same properties can still be generated.

## Installation

simSPT is written in plain C and distributed with autotools, and in theory should be able to compile on any platform. Compilation is performed using GCC. It depends on the following libraries:
- [libgsl](https://www.gnu.org/software/gsl/) (Debian/Ubuntu: `sudo apt install libgsl-dev`--  Mac/[Homebrew](https://brew.sh/) `brew install gsl` 
- libmeschach (Debian/Ubuntu: `sudo apt install libmeschach1.2 libmeschach-dev`)

```
autoreconf -i
./configure
make
```

Some parameters are accessible as command-line arguments (see below: command-line parameters). Other parameters (documented below) can be accessed by directly updated in `simSPT.c` and the code has to be recompiled. Compilation can be achieved by typing:

```{shell}
make
```

## Usage
Then, to launch simSPT, simply type: `./simSPT`. In that case, default parameters are used and printed, and the resulting simulation is saved in the `out.txt` file.

### Hardcoded parameters
Some parameters are not accessible through the command-line, but can be freely tuned. To do so, you have to edit the `simSPT.c` file *and recompile*. Note that the non-command line parameters are not printed when you start the program, thus you have to be careful when changing them.

### Command-line parameters
#### General parameters
Some parameters (but not all) of simSPT are available through command-line options. Here are the available parameters so far:

|Command line option| Unit |
|-------------------|------|
| -sigma            | µm   |
| -dt               | s	   |
| -n_traj           |	   |
| -file             |	   |
| -seed             |	   |
| -radius           | µm   |

**Note:** you need to use equal `=` signs and no space between the parameter name. Also, note that the parameters are prefixed by a single dash `-`, instead of the traditional double dash `--`.
Finally, always review the parsed parameters displayed by simSPT before running a simulation, to make sure that the parameters were properly interpreted.

#### Specifying subpopulations
Subpopulations are characterized by a diffusion coefficient. simSPT either supports coexisting subpopulations without state changes (one particle always remains in the same state) or with state changes. In the latter case, a matrix of state transitions has to be provided.

**No state transitions** 
Diffusion coefficients are specified by the `-Dxx=` syntax, and are expressed in µm²/s. The proportions (specified by the `-pxx=` syntax) should all be specified (for instance, when simulating a 2-states model, all `-D1=.. -D2=.. -p1=.. -p2=..` should be specified).

|Command line option| Unit |
|-------------------|------|
| -D1               | µm²/s|
| -D2               | µm²/s|
| -p1               |      |
| -p2               |      |
					       
**State transitions**
As without state transitions, the diffusion coefficient of the various subpopulations should be specified with the `-Dxx` syntax. Then, the `-kxx_yy=` option allows to specify the transition rate from the state `xx` to the state `yy`. Transition rates should be specified in 1/s (seconds^-1). The state transitions not specified are assumed to be zero. Before performing the simulation, the software will display the matrix of transition rates.

**Note:** the `-p` and `-k` options are incompatible.

|Command line option| Unit |
|-------------------|------|
| -D1               | µm²/s|
| -D2               | µm²/s|
| -k1_2             | 1/s  |
| -k2_1             | 1/s  |

#### Generating clusters/heterogeneous diffusion

simSPT has basic support for diffusion of a particle in a heterogeneous medium. This support so far is limited to the following hypothesis:
- Depending on which region of space it resides, the particle has a different fraction bound and enrichment compared to the baseline of the nucleus
- There is no obstacle to obstacle between regions of space
- Those regions of space can be specified using a specific clusters file

To generate a clusters file, use `simSPT makeclusters`.

To simulate according to a generated clusters file, use for instance `./simSPT -D1=0.01 -D2=1 -p1=0.5 -p2=0.5 -dt=0.013 -clusters=./heterogeneous/2.txt`

A Jupyter notebook located in the `./heterogeneous` folder presents a more thorough introduction to the generation of SPT trajectories in a nucleus that contains clusters.

#### Simulating anomalous diffusion
simSPT has basic capabilities to simulate anomalous diffusion. So far, only one model of anomalous diffusion is implemented, namely fractional Brownian motion. Simulation is performed using C implementations developed by Ton Diecker () and released under MIT license (I think). 

State changes are not implemented when anomalous diffusion has been selected. Fractional Brownian motion has only one parameter, the Hurst parameter $H$ (that is equal to half of the anomalous diffusion parameter, that is $H=\alpha/2$.

A small example is specified below:

```{shell}
./simSPT -motion=fbm -H=0.2 -D1=3.0 -p1=1.0 -n_traj=50000 -sigma=0 -file=./outL2.txt -seed=22
```

**Caveats:** A few limitations should be taken into account before using this implementation of Fractional Brownian motion. 

Each trajectory is simulated in a buffer. Sometimes, the buffer is too small, causing the program to crash (note that this could be handled by the code, but is not implemented so far). In that case, edit the `simSPT.c` source file and double the value of the `fbm_bufsize` variable (around line 68).

#### Examples
**No state changes**
Simulate a two states model, with a slow fraction diffusing at 0.001 µm²/s, a fast fraction diffusing at 1 µm²/s, and a relative fraction of 50/50%. The localization error (`-sigma`) is set to 35 nm, the frame rate to 10 ms (`-dt`). 100000 trajectories are simulated and stored in `170621_D1_p0.5_dt0.001s.csv`. The pseudo-random number generator (PRNG) is initialized at 0 (meaning that this simulations is reproducible, and will always provide the same result). This option can be removed to get a random initialization of the PRNG.

```{shell}
./simSPT -D1=0.001 -D2=1.0 -p1=0.5 -p2=0.5 -sigma=0.035 -dt=0.001 -n_traj=100000 -file=170621_D1_p0.5_dt0.001s.csv -seed=0
```

**With state changes**
This will simulate exactly the same set of parameters as the previous example, but now state transitions are simulated, and:

- $`k_{1 \to 2}=25.0 s^{-1}`$ (`k1_2=25.0`)
- $`k_{2 \to 1}=25.0 s^{-1}`$ (`k2_1=25.0`)


```{shell}
./simSPT -D1=0.001 -D2=1.0 -k1_2=25.0 -k2_1=25.0 -sigma=0.035 -dt=0.001 -n_traj=100000 -file=170621_D1_p0.5_dt0.001s.csv -seed=0
```

Again, in this example the seed of the pseudo random number generator is specified: every run of this function will always provide the same result, which can be useful for testing purposes.

**Saving a log file**
The output displayed in the terminal can be redirected to a logfile, by using a file redirection `> logfile.log`, for instance:

```{shell}
./simSPT -D1=0.001 -D2=1.0 -k1_2=25.0 -k2_1=25.0 -sigma=0.035 -dt=0.001 -n_traj=100000 -file=D1_p0.5_dt0.001s.csv -seed=0 > D1_p0.5_dt0.001s.log
```

will save the detections to `D1_p0.5_dt0.001s.csv`, and the log output will be saved on `D1_p0.5_dt0.001s.log`. In that case, nothing is displayed on the terminal. To both display the log in the terminal and save it on file, use the `tee` command:

```{shell}
./simSPT -D1=0.001 -D2=1.0 -k1_2=25.0 -k2_1=25.0 -sigma=0.035 -dt=0.001 -n_traj=100000 -file=D1_p0.5_dt0.001s.csv -seed=0 | tee D1_p0.5_dt0.001s.log
```

**Heterogeneous clusters**
More detailed examples are in the `heterogeneous` subfolder, including a README file and a Jupyter notebook detailing a few of the configurations.

### Extra functions

- Display the detection profile in z as a CSV file: `./simSPT zprofile`

## Documentation
### Options reference
*simSPT* parameters are grouped by categories:

#### `geometry_params`: parameters specifying the confining geometry

- `geometry` (int): Set to 0 to simulate in a sphere, to 1 to simulate in a cube
- `radius` (double): the radius of the sphere, or 2x the width of the cube (in µm). Available through the command-line.

#### `hilo_params`: parameters specifying the HiLo beam
The HiLo beam is assumed to be parallel to the (x,y) plane, has the shape of a step function. Inside the HiLo beam, particles are progressively bleached. Outside the HiLo beam, they bleach with rate 1/(bleachProb*lifetime).

- `width` (float): the width of the HiLo beam (in µm).
- `bleachProb` (float): the probability for a particle outside the Hilo beam to "age".

This weird formulation of ageing adopts the formalism of *exponential clocks*: every time a particle is created, the software associate a lifetime. This lifetime is an integer number of frames that is drawn depending on the lifetime parameter. Then, every frame where the particle lies within the HiLo volume, this lifetime parameter is decremented by one. When it reaches zero, the particles is considered as bleached and is removed from the simulation. Whenever the particle is outside the HiLo volume, its bleaching probability is not zero (and thus the probability of the "clock" to "tick", and the remaining lifetime of the particle). This is implemented by this `bleachProb` parameter, that reflects the probability per frame that a particle outside the HiLo volume "ages".

This parameter has to be set to zero if you want bleaching to occur only in the HiLo volume. Note however, that if you set `bleachProb` to zero, bound/extremely slow moving particles outside the HiLo volume will take an extremely long time to bleach, as it will take them litteraly ages to ever reach the HiLo volume. 

#### `detection_params`: parameters relative to the detection profile in z

- `profileType` (int): set to 0 for a Gaussian detection profile, 1 for a flat-top Gaussian
- `fwhm` (float): the full width at half maximum (FWHM ~= 2.35 sigma )of the Gaussian (or of the Gaussian boundaries of the flat-top Gaussian). in µm.
- `plateau` (float): only used for the flat-top Gaussian case. The width of the flat-top (in µm).
- `resolution` (int): the number of points where the detection profile will be evaluated. It is reasonable to leave the default parameter.

#### `pops`: specification of the populations
Populations are defined by a diffusion coefficient (D), a proportion (p), a fluorophore lifetime (beta). Each element of the `pops` object corresponds to a different population.

- `n_pops` (int): number of diffusing populations
- `p` (float): the proportion of this population. Note that the p have to sum to one, else a warning will be issued.
- `D` (float): the diffusion coefficient of the population (in µm²/s)
- `beta` (float): the mean lifetime of a fluorophore (the mean parameter of an exponential distribution). Unit: either seconds or frames, depending on the `betaUnit` parameter
- `betaUnit` (int): the unit of `beta`. Set to 0 for frames and to 1 for seconds.


#### `simulation_params`: parameters relative to the observation/simulation

- `dt`: the time interval between frames (in seconds)
- `gaps`: the number of gaps allowed in tracking
- `locerror`: the localization error (in µm).

#### Save and export parameters

- `path` (string): the path where to save the simulations as CSV
- `save` (bool): set to true to actually save something.

### Methods
**Pseudo-random number generator (PRNG):**: simSPT uses an updated version of the Mersenne-Twister algorithm (as implemented in the GNU Scientific Library –GSL–). Prior to version 1.6, simSPT was using the `rand()` function of the C standard lib. Although this PRNG is not too bad, it is regarded as suboptimal.

**Gaussian random number transformation**: simSPT uses Marsaglia's ziggurat method to generate Gaussian numbers. Prior to version 1.6, simSPT was using the Marsaglia Polar Method.

**Diffusion & confinment:** simSPT simulates 3D brownian diffusion using the Euler-Maruyama scheme. Confinement is achieved by performing specular reflections on the limits of the simulation volume (see https://math.stackexchange.com/questions/225614/tangent-plane-to-sphere and https://en.wikipedia.org/wiki/Specular_reflection#Vector_formulation for implementation details).

**Bleaching & z profile:** Particles are drawn at random from one of the populations and are simulated until they bleach. Bleaching occurs when the number of steps the particle has been inside the HiLo beam reaches the value of an exponential clock initialized when the particle was first drawn. Particles are observed with a probability defined by the detection profile.

**Steady-state computation:** when transition states are specified, simSPT computes the equilibrium fractions ("relative concentrations") achieved under this matrix of transition states. The steady-state distribution is determined by extracting the discrete Markov Chain probability matrix $`P`$ from the matrix of transition rates $`Q`$. This is done by normalizing the matrix by the transition rates.

Then, the discrete-time steady state $`\pi`$ is determined as the solution of the $`\pi P = \pi`$ equation. The continuous-time steady state is finally derived by renormalizing $`\pi`$ by the transition rates.


## Acknowledgements
We are very thankful to Anders S. Hansen who designed the initial simulation scheme, for comments and insightful suggestions.

simSPT incorporates read.c from Scott Brueckner (Sept. 1999).
