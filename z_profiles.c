#include "simSPT_structs.h"
#include "z_profiles.h"

struct zprofileInit initialize_z_profile(struct detectionPars pars,
					 struct geometryInit geometry_init,
					 struct zprofileInit *zprofile_init)
{
  // init
  int i;
  float stp = (geometry_init.max_z-geometry_init.min_z)/(float)pars.resolution;
  float mz = geometry_init.min_z;
  float M = 0;
  // Instanciate return struct
  //struct zprofileInit *zprofile_init = malloc(sizeof(struct zprofileInit));
  if (zprofile_init == NULL) {
    printf("Could not allocate the z_profile object. This smells bad. Segfault on the way :s");
  };

  // Instanciate the computation of the z profile
  zprofile_init->z_prob = malloc((pars.resolution+1)*sizeof(float));
  zprofile_init->min_z = mz;
  zprofile_init->step = stp;

  for (i=0;i<pars.resolution+1;i++) {
    // Compute the real x value
    // Get the value depending on the profileType
    if (pars.profileType==0) { // Gaussian profile
      zprofile_init->z_prob[i] = normal(i*stp+mz, 0, pars.fwhm);
    } else if (pars.profileType==1) { // Flat-Top-Gaussian
      zprofile_init->z_prob[i] = flattopnormal(i*stp+mz, pars.fwhm, pars.plateau);
    } else {
      printf("The profileType parameter of the detection_params has an invalid value. SEGFAULT on the way :s.");
    }
    if (zprofile_init->z_prob[i] > M) {
      M = zprofile_init->z_prob[i];
    }
  }
  for (i=0;i<pars.resolution+1;i++) {
    zprofile_init->z_prob[i]/= M;
    //printf("%f %f\n", i*stp+mz, zprofile_init->z_prob[i]);
  }
  return *zprofile_init;
};

float normal(float x, float mu, float fwhm) {
  float sigma = fwhm/(2*log(2));
  return 1/(sigma*sqrt(2*M_PI))*exp(-pow(x-mu,2)/(2*pow(sigma,2)));
}

float flattopnormal(float x, float fwhm, float plateau) {
  if ((x>-plateau/2.0) && (x<plateau/2.0)) {
    return normal(0, 0, fwhm);
  } else {
    return normal(fabs(x)-plateau/2.0, 0, fwhm);
  }
}

void print_zprofile(struct detectionPars pars,
		    struct geometryInit geometry_init,
		    struct zprofileInit z_profile) {
  int i=0;
  float stp = (geometry_init.max_z-geometry_init.min_z)/(float)pars.resolution;
  float mz = geometry_init.min_z;

  printf("z,p\n");
  for (i=0;i<pars.resolution;i++) {
    printf("%f,%f\n", i*stp+mz, z_profile.z_prob[i]);
  }
}
